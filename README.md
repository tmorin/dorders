# dorders

> A decentralized system helping local shops to handle orders.

## The paper

- [dorders-paper.pdf](https://framagit.org/tmorin/dorders/-/jobs/artifacts/feat/implement-proto/raw/dist/dorders-paper.pdf?job=generate-paper)
