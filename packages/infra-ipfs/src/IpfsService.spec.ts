import {IpfsConfig, IpfsConfigScope, IpfsService} from '.';
import {InMemoryConfigProvider} from '@dorders/infra-config-inmemory';
import {LogpleaseLoggerFactory} from '@dorders/infra-logger-logplease';

jest.setTimeout(15000);

describe('IpfsService', function () {

  it('should get and stop', async function () {
    const configProvider0 = new InMemoryConfigProvider();
    await configProvider0.patch<IpfsConfig>(IpfsConfigScope, {
      options: {silent: true, init: {profiles: ['test']}},
      repository: 'tmp/container-0/ipfs'
    });
    const ipfsService0 = new IpfsService(configProvider0, new LogpleaseLoggerFactory());
    await ipfsService0.get();
    await ipfsService0.dispose();
  });

})
