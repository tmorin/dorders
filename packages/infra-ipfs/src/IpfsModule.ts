import {
  AbstractModule,
  ConfigProvider,
  ConfigProviderSymbol,
  LoggerFactory,
  LoggerFactorySymbol
} from '@dorders/framework';
import {IpfsService, IpfsServiceSymbol} from './IpfsService';

export class IpfsModule extends AbstractModule {

  async configure(): Promise<void> {

    this.registry.registerFactory(
      IpfsServiceSymbol,
      registry => new IpfsService(
        registry.resolve<ConfigProvider>(ConfigProviderSymbol),
        registry.resolve<LoggerFactory>(LoggerFactorySymbol)
      ),
      {
        singleton: true
      });

  }

  async dispose(): Promise<void> {
    await this.registry.resolve<IpfsService>(IpfsServiceSymbol).dispose();
  }

}
