import IPFS, {IpfsApi} from 'ipfs';
import defaultsdeep from 'lodash.defaultsdeep';
import {ConfigProvider, Logger, LoggerFactory} from '@dorders/framework';
import {IpfsConfig, IpfsConfigScope} from './IpfsConfig';

export {IpfsApi} from 'ipfs';

export const IpfsServiceSymbol = Symbol.for('IpfsService');

export class IpfsService {

  private readonly ipfs: Promise<IpfsApi>;

  private readonly logger: Logger = this.loggerFactory.create(IpfsService.name);

  constructor(
    private readonly configProvider: ConfigProvider,
    private readonly loggerFactory: LoggerFactory
  ) {
    const config = this.configProvider.get<IpfsConfig>(IpfsConfigScope);

    this.logger.info('create IPFS new (%s)', config.repository);

    const baseConfig = {
      repo: config.repository,
      EXPERIMENTAL: {
        pubsub: true
      }
    };

    const finalConfig = defaultsdeep({}, config.options || {}, baseConfig);

    this.ipfs = IPFS.create(finalConfig);
  }

  async get(): Promise<IpfsApi> {
    return await this.ipfs;
  }

  async dispose(): Promise<void> {
    const config = this.configProvider.get<IpfsConfig>(IpfsConfigScope);
    this.logger.info('stopping IPFS (%s)', config.repository);
    try {
      const ipfs = await this.get();
      await ipfs.stop();
    } catch (e) {
      this.logger.error('unable to stop IPFS', e);
    } finally {
      this.logger.info('IPFS (%s) stopped', config.repository);
    }
  }
}
