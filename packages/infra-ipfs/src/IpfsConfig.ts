import {IpfsOptions} from 'ipfs';

export const IpfsConfigScope = Symbol.for('ipfs')

export interface IpfsConfig {
  repository: string
  options?: IpfsOptions
}
