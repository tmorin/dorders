import {Container} from '@dorders/framework';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {startIpfsContainers} from './index';
import {disposeContainers} from '@dorders/infra-test';

jest.setTimeout(15000);

describe('IpfsService-files', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startIpfsContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should share content', async function () {
    const ipfs0 = await container0.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
    const ipfs1 = await container1.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
    expect(ipfs0.isOnline()).toBeTruthy();
    expect(ipfs1.isOnline()).toBeTruthy();
    expect((await ipfs0.swarm.peers()).length).toEqual(1);
    expect((await ipfs1.swarm.peers()).length).toEqual(1);

    let pAIpfs: string;
    {
      console.debug('-- from ipfs0');
      await ipfs0.files.write('/orders/profiles/pA/manifest.json', JSON.stringify({id: 'pA'}), {
        create: true, parents: true
      });
      await ipfs0.files.write('/orders/profiles/pA/README.md', '# hello', {
        create: true, parents: true
      });
      const pA = await ipfs0.files.stat('/orders/profiles/pA');
      console.debug('/orders/profiles/pA', pA.size, pA.cid.toString(), pA.cid.version);
      pAIpfs = '/ipfs/' + pA.cid.toString();
      const readme = await ipfs0.files.stat('/orders/profiles/pA/README.md');
      console.debug('/orders/profiles/pA/README.json', readme.size, readme.cid.toString(), readme.cid.version);
    }

    {
      console.debug('-- from ipfs1');
      const pA = await ipfs0.files.stat(pAIpfs);
      console.debug('/orders/profiles/pA', pA.size, pA.cid.toString(), pA.cid.version);
    }
  });

});

