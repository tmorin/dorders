import {CreateOptions, getRunningContainers, startContainers, waitFor} from '@dorders/infra-test';
import {Container, Module} from '@dorders/framework';
import {IpfsModule, IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {InfraTestIpfsModule} from './InfraTestIpfsModule';
import {LogpleaseLoggerFactory} from '@dorders/infra-logger-logplease';

export async function startIpfsContainers(
  numbers: number,
  options: Partial<CreateOptions> = {},
  modulesFactory: () => Array<Module> = () => ([])
): Promise<Array<Container>> {
  const containers = await startContainers(
    numbers,
    {
      ...options,
      loggerFactory: new LogpleaseLoggerFactory()
    },
    () => ([new InfraTestIpfsModule(), new IpfsModule(), ...modulesFactory()])
  );
  await linkNodes(options);
  return containers;
}

export async function linkNodes(options: Partial<CreateOptions> = {}) {
  if (options.verbose) {
    console.debug('link (%s) nodes', getRunningContainers().length);
  }
  for (const container0 of getRunningContainers()) {
    const ipfs0 = await container0.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
    for (const container1 of getRunningContainers()) {
      if (container0 !== container1) {
        const ipfs1 = await container1.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
        const ipfs1Id = await ipfs1.id();
        await ipfs0.swarm.connect(ipfs1Id.addresses[0]);
        await waitFor(200); // wait for the linkage
      }
    }
  }
}
