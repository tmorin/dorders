import {AbstractModule, LoggerFactory, LoggerFactorySymbol} from '@dorders/framework';
import {ConfigsTestProviderSymbol} from '@dorders/infra-test';
import {IpfsConfigsTestProvider} from './IpfsConfigsTestProvider';

export class InfraTestIpfsModule extends AbstractModule {

  async configure(): Promise<void> {
    this.registry.registerValue(ConfigsTestProviderSymbol, new IpfsConfigsTestProvider(
      this.registry.resolve<LoggerFactory>(LoggerFactorySymbol)
    ));
  }

}
