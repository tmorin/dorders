import {Container} from '@dorders/framework';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {startIpfsContainers} from '.';
import {disposeContainers} from '@dorders/infra-test';

jest.setTimeout(15000);

describe('IpfsService', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async () => {
    [container0, container1] = await startIpfsContainers(2);
  });
  afterEach(async () => {
    await disposeContainers();
  });

  it('should be scoped by container', async function () {
    const ipfsService0 = container0.registry.resolve<IpfsService>(IpfsServiceSymbol);
    const ipfsService0Bis = container0.registry.resolve<IpfsService>(IpfsServiceSymbol);
    const ipfsService1 = container1.registry.resolve<IpfsService>(IpfsServiceSymbol);
    expect(ipfsService0).toEqual(ipfsService0Bis);
    expect(ipfsService0).not.toEqual(ipfsService1);
  });

})
