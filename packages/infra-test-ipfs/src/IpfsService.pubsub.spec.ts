import {Container} from '@dorders/framework';
import {disposeContainers, waitFor} from '@dorders/infra-test';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {startIpfsContainers} from './index';

jest.setTimeout(15000);

describe('IpfsService-pubsub', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startIpfsContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should share topics', async function () {
    const ipfs0 = await container0.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
    const ipfs0Id = await ipfs0.id();
    const ipfs1 = await container1.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
    const ipfs1Id = await ipfs1.id();
    expect(ipfs0.isOnline()).toBeTruthy();
    expect(ipfs1.isOnline()).toBeTruthy();

    const msgGetByIpfs0 = [];
    await ipfs0.pubsub.subscribe('topic', msg => msgGetByIpfs0.push(msg));

    const msgGetByIpfs1 = [];
    await ipfs1.pubsub.subscribe('topic', msg => msgGetByIpfs1.push(msg));

    await waitFor(200);

    console.debug(ipfs0Id.id, await ipfs0.pubsub.peers('topic'));
    console.debug(ipfs1Id.id, await ipfs1.pubsub.peers('topic'));

    expect([ipfs1Id.id]).toEqual(await ipfs0.pubsub.peers('topic'));
    expect([ipfs0Id.id]).toEqual(await ipfs1.pubsub.peers('topic'));

    await ipfs0.pubsub.publish('topic', 'from ipfs0');
    await ipfs1.pubsub.publish('topic', 'from ipfs1');
    await ipfs0.pubsub.publish('topic', 'from ipfs0');

    await waitFor(300);

    console.debug(ipfs0Id.id, 'msgFromIpfs0', msgGetByIpfs0);
    console.debug(ipfs1Id.id, 'msgFromIpfs1', msgGetByIpfs1);

    expect(3).toEqual(msgGetByIpfs0.length);
    expect(3).toEqual(msgGetByIpfs1.length);
  });

});
