import {startIpfsContainers} from './container';
import {ConfigProviderSymbol, LoggerFactorySymbol, MessageBusSymbol} from '@dorders/framework';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {disposeContainers} from '@dorders/infra-test';

jest.setTimeout(15000);

describe('main', function () {

  it('should have a configured container', async function () {
    const [container0, container1] = await startIpfsContainers(2);
    expect(container0.registry.resolve(MessageBusSymbol)).toBeTruthy();
    expect(container1.registry.resolve(MessageBusSymbol)).toBeTruthy();
    expect(container0.registry.resolve(LoggerFactorySymbol)).toBeTruthy();
    expect(container0.registry.resolve(ConfigProviderSymbol)).toBeTruthy();
    const ipfsService0 = container0.registry.resolve<IpfsService>(IpfsServiceSymbol);
    expect(await ipfsService0.get()).toBeTruthy();
    const ipfsService1 = container1.registry.resolve<IpfsService>(IpfsServiceSymbol);
    expect(await ipfsService1.get()).toBeTruthy();
    expect(ipfsService0).not.toBe(ipfsService1);
    await disposeContainers();
  });

})
