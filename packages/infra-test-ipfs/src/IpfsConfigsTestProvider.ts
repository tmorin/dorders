import {ConfigsTestProvider} from '@dorders/infra-test';
import {Config, Logger, LoggerFactory} from '@dorders/framework';
import {IpfsConfigScope} from '@dorders/infra-ipfs';

export class IpfsConfigsTestProvider extends ConfigsTestProvider {

  private readonly logger: Logger = this.loggerFactory.create(IpfsConfigsTestProvider.name);

  constructor(
    private readonly loggerFactory: LoggerFactory
  ) {
    super();
  }

  async provide(index: number): Promise<Map<symbol, Config>> {
    const map = new Map();
    this.logger.debug('index (%s) - set test config for scope (%s)', index, IpfsConfigScope);
    map.set(IpfsConfigScope, {
      repository: `tmp/container-${index}/ipfs`,
      options: {
        silent: true,
        init: {
          profiles: ['test']
        },
        config: {
          Bootstrap: [],
          Addresses: {
            Swarm: [
              `/ip4/0.0.0.0/tcp/310${index}0`
            ],
            API: `/ip4/0.0.0.0/tcp/310${index}1`,
            Gateway: `/ip4/0.0.0.0/tcp/310${index}2`,
            // Delegates: []
          }
        }
      }
    });
    return map;
  }

}
