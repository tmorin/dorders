import {
  PrivateProfileRepository,
  PrivateProfileRepositorySymbol,
  ProfileId,
  PublicProfileReference, SerializedPublicProfileReference
} from '@dorders/model-profile';
import {Container} from '@dorders/framework';

export async function getPublicReference(container: Container, profileId: ProfileId): Promise<PublicProfileReference> {
  const repository = container.registry.resolve<PrivateProfileRepository>(PrivateProfileRepositorySymbol);
  const profile = await repository.get(profileId);
  return await profile.publicProfile.getReference();
}

export async function getSerializedPublicReference(container: Container, profileId: ProfileId): Promise<SerializedPublicProfileReference> {
  const reference = await getPublicReference(container, profileId);
  return await reference.serialize();
}
