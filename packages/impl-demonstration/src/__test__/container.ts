import {CreateOptions, startContainers} from '@dorders/infra-test';
import {ModelPeerModule} from '@dorders/model-peer';
import {InfraPeerModule} from '../peer';
import {ConsoleLogger, Level} from '@dorders/infra-logger-console';
// import {InfraContactModule} from '../contact';
// import {InfraProfileModule} from '../profile';

ConsoleLogger.DEFAULT_LEVEL = Level.warn;

export async function startDemoContainers(
  numbers: number,
  options: Partial<CreateOptions> = {}
) {
  return await startContainers(numbers, options, () => ([
    new ModelPeerModule(), new InfraPeerModule(),
    // new ModelProfileModule(), new InfraProfileModule(),
    // new ModelContactModule(), new InfraContactModule(),
  ]))
}
