import * as uuid from 'uuid';
import {SimplePrivateProfile} from './SimplePrivateProfile';
import {SimplePrivateProfileReference} from './SimplePrivateProfileReference';
import {ConfigProvider, Logger, LoggerFactory} from '@dorders/framework';
import {PrivateProfile, PrivateProfileFactory, PrivateProfileReference, ProfileId} from '@dorders/model-profile';
import {ProfileConfig, ProfileConfigScope} from './ProfileConfig';

export class SimplePrivateProfileFactory implements PrivateProfileFactory {

  private readonly logger: Logger = this.loggerFactory.create(`profile/${SimplePrivateProfileFactory.name}`);

  private readonly repositoryId;

  constructor(
    private readonly configProvider: ConfigProvider,
    private readonly loggerFactory: LoggerFactory,
    private readonly cache: Map<ProfileId, SimplePrivateProfile> = new Map()
  ) {
    const config: ProfileConfig = configProvider.get(ProfileConfigScope)
    this.repositoryId = config.repositoryId
  }

  async createFromScratch(): Promise<PrivateProfile> {
    const profileId: ProfileId = uuid.v4();
    this.logger.info('create profile (%s) from scratch', profileId);
    return this.putToCache(new SimplePrivateProfile(
      profileId,
      this.repositoryId,
      this.logger
    ));
  }

  async createFromReference(privateProfileReference: PrivateProfileReference): Promise<PrivateProfile> {
    const profileId = privateProfileReference.profileId;

    if (this.cache.has(profileId)) {
      return this.cache.get(profileId);
    }

    const reference = SimplePrivateProfileReference.from(privateProfileReference);

    this.logger.info('create profile (%s) from reference', reference.profileId);

    this.logger.info('profile (%s) created', profileId);

    return this.putToCache(new SimplePrivateProfile(
      profileId,
      this.repositoryId,
      this.logger
    ));
  }

  async dispose() {
    this.logger.info('dispose the profile cache')
    for (const simplePrivateProfile of this.cache.values()) {
      try {
        await simplePrivateProfile.stop();
      } catch (e) {
        this.logger.debug('unable to stop a SimplePrivateProfile', e.message);
      }
    }
    this.cache.clear();
  }

  private putToCache(simplePrivateProfile: SimplePrivateProfile) {
    this.cache.set(simplePrivateProfile.profileId, simplePrivateProfile);
    return simplePrivateProfile.once(
      SimplePrivateProfile.EVENT_CLOSED,
      () => this.cache.delete(simplePrivateProfile.profileId)
    );
  }

}
