export const ProfileConfigScope = Symbol.for('profile')

export interface ProfileConfig {
  repositoryId: string
}
