import {EventEmitter} from 'events';
import {Logger} from '@dorders/framework';
import {
  PrivateProfile,
  PrivateProfileReference,
  ProfileCardUpdated,
  ProfileCreated,
  ProfileDeleted,
  ProfileId,
  ProfileSynchronized
} from '@dorders/model-profile';
import {SimplePrivateProfileReference} from './SimplePrivateProfileReference';
import {ProfileProperty} from './ProfileProperty';
import {SimplePublicProfile} from './SimplePublicProfile';

export class SimplePrivateProfile extends EventEmitter implements PrivateProfile {

  static readonly EVENT_CLOSED = Symbol.for('closed');

  constructor(
    public readonly profileId: ProfileId,
    public readonly repositoryId: string,
    public readonly logger: Logger,
    public readonly privateMap: Map<string, string> = new Map<string, string>(),
    public readonly publicMap: Map<string, string> = new Map<string, string>(),
    public readonly publicProfile = new SimplePublicProfile(profileId, repositoryId, publicMap)
  ) {
    super();
  }

  static from(value: PrivateProfile): SimplePrivateProfile {
    if (value instanceof SimplePrivateProfile) {
      return value;
    } else {
      throw new Error('unable to cast to SimplePrivateProfile');
    }
  }

  async getReference(): Promise<PrivateProfileReference> {
    return new SimplePrivateProfileReference(this.profileId, this.repositoryId);
  }

  // profile

  async applyProfileCreated(profileCreated: ProfileCreated): Promise<void> {
    this.logger.debug('the profile (%s) has been created', this.profileId);
  }

  async applyProfileCardUpdated(profileCardUpdated: ProfileCardUpdated): Promise<void> {
    this.logger.debug('the card of the profile (%s) has been updated', this.profileId);
    const profileCard = profileCardUpdated.body.profileCard;
    const profileCardAsString = typeof profileCard === 'string' ? profileCard : JSON.stringify(profileCard);
    await this.publicMap.set(ProfileProperty.profile_card, profileCardAsString);
  }

  async applyProfilesSynchronized(profilesSynchronized: ProfileSynchronized): Promise<void> {
    this.logger.debug('the profile (%s) has been synchronized', this.profileId);
  }

  async applyProfileDeleted(deleted: ProfileDeleted): Promise<void> {
    this.logger.debug('the profile (%s) has been deleted', this.profileId);
    await this.destroy();
  }

  async stop() {
    this.emit(SimplePrivateProfile.EVENT_CLOSED);
    this.removeAllListeners();
  }

  async destroy() {
    await this.stop();
  }

}
