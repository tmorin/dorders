import {Logger, LoggerFactory, MessageBus} from '@dorders/framework';
import {PrivateProfile, ProfileSynchronizerService} from '@dorders/model-profile';

export class SimpleProfileSynchronizerService implements ProfileSynchronizerService {

  private readonly logger: Logger = this.loggerFactory.create(`profile/${SimpleProfileSynchronizerService.name}`);

  constructor(
    private readonly messageBus: MessageBus,
    private readonly loggerFactory: LoggerFactory
  ) {
  }

  async startOngoingSynchronization(profile: PrivateProfile): Promise<void> {
    this.logger.debug('start ongoing synchronization for profile (%s)', profile.profileId);
  }

}
