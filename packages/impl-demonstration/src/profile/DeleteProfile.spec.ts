import {Container} from '@dorders/framework';
import {disposeContainers, waitForOnce} from '@dorders/infra-test';
import {
  CreateProfile,
  DeleteProfile,
  PrivateProfileRepository,
  PrivateProfileRepositorySymbol,
  ProfileCreated,
  ProfileDeleted
} from '@dorders/model-profile';
import {startDemoContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('DeleteProfile', function () {

  let container0: Container;
  beforeEach(async function () {
    [container0] = await startDemoContainers(1);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should succeed', async function () {
    const pWaitForEvents = waitForOnce(container0, ProfileDeleted.EVENT_NAME);

    const [profileACreated] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'ProfileA'}));
    expect(profileACreated).toBeTruthy();
    expect(profileACreated.body.profileId).toBeTruthy();

    const [profileBCreated] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'ProfileB'}));
    expect(profileBCreated).toBeTruthy();
    expect(profileBCreated.body.profileId).toBeTruthy();

    const [profileCCreated] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'ProfileC'}));
    expect(profileCCreated).toBeTruthy();
    expect(profileCCreated.body.profileId).toBeTruthy();

    const privateProfileRepository0 = await container0.registry.resolve<PrivateProfileRepository>(PrivateProfileRepositorySymbol);

    const beforeProfiles = await privateProfileRepository0.list();
    expect(beforeProfiles.length).toEqual(3);

    const [profileBDeleted] = await container0.messageBus.execute<ProfileDeleted>(new DeleteProfile({profileId: profileBCreated.body.profileId}));
    expect(profileBDeleted).toBeTruthy();
    expect(profileBDeleted.body.profileId).toEqual(profileBCreated.body.profileId);

    const afterProfiles = await privateProfileRepository0.list();
    expect(afterProfiles.length).toEqual(2);

    await pWaitForEvents;
  });
});

