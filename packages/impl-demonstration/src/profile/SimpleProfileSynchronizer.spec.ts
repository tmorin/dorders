import {Container} from '@dorders/framework';
import {disposeContainers, waitForOnce} from '@dorders/infra-test';
import {
  CreateProfile,
  ImportProfile,
  PrivateProfileRepository,
  PrivateProfileRepositorySymbol,
  ProfileCreated,
  ProfileSynchronized,
  UpdateProfileCard
} from '@dorders/model-profile';
import {startDemoContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('SimpleProfileSynchronizer', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startDemoContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should start the ongoing synchronization ', async function () {
    const [profileACreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));
    const profileA0 = await container0.registry.resolve<PrivateProfileRepository>(PrivateProfileRepositorySymbol).get(profileACreated0.body.profileId);
    const profileARef0 = await profileA0.getReference();
    const profileARefAsString = await profileARef0.serialize();

    let waitForSynchronization = waitForOnce(container1, ProfileSynchronized.EVENT_NAME);
    await container1.messageBus.execute<ProfileCreated>(new ImportProfile({serializedReference: profileARefAsString}));
    await waitForSynchronization;

    waitForSynchronization = waitForOnce(container1, ProfileSynchronized.EVENT_NAME);
    await container0.messageBus.execute(new UpdateProfileCard({
      profileId: profileACreated0.body.profileId,
      profileCard: 'profile card bis'
    }))
    await waitForSynchronization;
  });

});
