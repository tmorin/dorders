import {SimplePublicProfileReference} from './SimplePublicProfileReference';
import {
  PublicProfileReference,
  PublicProfileReferenceDeserializer,
  SerializedPublicProfileReference
} from '@dorders/model-profile';

export class JsonPublicProfileReferenceDeserializer implements PublicProfileReferenceDeserializer {

  async deserialize(serializedPublicProfileReference: SerializedPublicProfileReference): Promise<PublicProfileReference> {
    const {profileId, repositoryId, name} = JSON.parse(serializedPublicProfileReference);
    return new SimplePublicProfileReference(profileId, repositoryId, name);
  }

}
