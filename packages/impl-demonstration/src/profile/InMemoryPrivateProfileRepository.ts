import * as uuid from 'uuid';
import {ProfileConfig, ProfileConfigScope} from './ProfileConfig';
import {SimplePrivateProfileReference} from './SimplePrivateProfileReference';
import {
  PrivateProfile,
  PrivateProfileFactory,
  PrivateProfileReference,
  PrivateProfileReferenceDeserializer,
  PrivateProfileRepository,
  ProfileId
} from '@dorders/model-profile';
import {ConfigProvider, Logger, LoggerFactory} from '@dorders/framework';

const PRIVATE_PROFILES = new Map<string, PrivateProfileReference>();

const REPOSITORIES = new Map<string, Map<string, PrivateProfileReference>>();

export class InMemoryPrivateProfileRepository implements PrivateProfileRepository {

  private readonly logger: Logger = this.loggerFactory.create(`profile/${InMemoryPrivateProfileRepository.name}`);

  private readonly map: Map<ProfileId, PrivateProfileReference>;

  private readonly identifier: string = uuid.v4();

  constructor(
    private readonly configProvider: ConfigProvider,
    private readonly privateProfileReferenceDeserializer: PrivateProfileReferenceDeserializer,
    private readonly privateProfileFactory: PrivateProfileFactory,
    private readonly loggerFactory: LoggerFactory
  ) {
    const config: ProfileConfig = configProvider.get(ProfileConfigScope)
    this.logger.debug('create LevelPrivateProfileRepository in (%s)', config.repositoryId);
    this.map = new Map();
    REPOSITORIES.set(this.identifier, this.map);
  }

  async add(privateProfile: PrivateProfile): Promise<void> {
    this.logger.debug('add the private profile (%s)', privateProfile.profileId);
    const privateProfileReference = await privateProfile.getReference();
    const reference = SimplePrivateProfileReference.from(privateProfileReference);
    const profileId = reference.profileId;
    if (this.map.has(profileId)) {
      throw new Error(`profile (${profileId}) already managed`);
    }
    this.map.set(profileId, reference);
  }

  async get(profileId: ProfileId): Promise<PrivateProfile> {
    let privateProfileReference;
    if (this.map.has(profileId)) {
      privateProfileReference = this.map.get(profileId);
      return await this.privateProfileFactory.createFromReference(privateProfileReference);
    }
    throw new Error(`profile (${profileId}) not found`);
  }

  iterate(): AsyncIterable<PrivateProfile> {
    const iterator = this.map.entries()[Symbol.asyncIterator]();
    const next = async () => {
      const result = await iterator.next();
      if (result.done) {
        return {done: true, value: null};
      }
      const profile = await this.get(result.value);
      return {done: false, value: profile};
    };
    return {
      [Symbol.asyncIterator]: () => ({next})
    }
  }

  async list(): Promise<Array<PrivateProfile>> {
    const privateProfiles: Array<PrivateProfile> = [];
    for (const profileId of this.map.keys()) {
      const privateProfile = await this.get(profileId);
      privateProfiles.push(privateProfile);
    }
    return privateProfiles;
  }

  async remove(privateProfile: PrivateProfile): Promise<void> {
    this.map.delete(privateProfile.profileId);
  }

  async dispose(): Promise<void> {
    this.logger.info('dispose the profile repository')
    this.map.clear();
    REPOSITORIES.delete(this.identifier);
  }

}
