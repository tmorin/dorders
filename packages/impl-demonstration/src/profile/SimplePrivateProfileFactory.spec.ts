import {SimplePrivateProfile} from './SimplePrivateProfile';
import {ProfileProperty} from './ProfileProperty';
import {SimplePrivateProfileReference} from './SimplePrivateProfileReference';
import {Container} from '@dorders/framework';
import {disposeContainer, disposeContainers, startContainers, waitFor} from '@dorders/infra-test';
import {ModelProfileModule, PrivateProfileFactory, PrivateProfileFactorySymbol} from '@dorders/model-profile';
import {ModelPeerModule} from '@dorders/model-peer';
import {InfraPeerModule} from '../peer';
import {InfraProfileModule} from './InfraProfileModule';
import {startDemoContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('SimplePrivateProfileFactory', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startDemoContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should create from scratch', async function () {
    const privateProfileFactory0 = container0.registry.resolve<PrivateProfileFactory>(PrivateProfileFactorySymbol);

    const profileA0 = await privateProfileFactory0.createFromScratch();
    expect(profileA0).toBeTruthy();

    const profileARef = await profileA0.getReference();

    expect(profileA0.profileId).toBeTruthy();
    expect(profileARef.profileId).toEqual(profileA0.profileId);

    const creationDate = SimplePrivateProfile.from(profileA0).publicMap.get(ProfileProperty.creation_date);
    expect(creationDate).toBeTruthy();
  });

  it('should create from reference', async function () {
    // node1
    const privateProfileFactory0 = container0.registry.resolve<PrivateProfileFactory>(PrivateProfileFactorySymbol);
    const profileA0 = await privateProfileFactory0.createFromScratch();
    const profileARef0 = await profileA0.getReference();
    const profileAPubRef0 = await profileA0.publicProfile.getReference();

    await waitFor(200);

    // node2
    const privateProfileFactory1 = container1.registry.resolve<PrivateProfileFactory>(PrivateProfileFactorySymbol);
    const profileA1 = await privateProfileFactory1.createFromReference(profileARef0);
    const profileARef1 = await profileA1.getReference();
    const profileAPubRef1 = await profileA1.publicProfile.getReference();

    await waitFor(200);

    // should import profileA from node 1 to node2 
    expect(await profileARef1.serialize()).toEqual(await profileARef0.serialize());
    expect(await profileAPubRef1.serialize()).toEqual(await profileAPubRef0.serialize());
  });

  it('should create from reference an hosted profile', async function () {
    const privateProfileFactory1 = container1.registry.resolve<PrivateProfileFactory>(PrivateProfileFactorySymbol);
    const profileA1 = await privateProfileFactory1.createFromScratch();
    const profileAPrvRef1 = await profileA1.getReference()

    await disposeContainer(1);
    await waitFor(200);
    [container1] = await startContainers(1, {clean: false}, () => ([
      new ModelPeerModule(), new InfraPeerModule(),
      new ModelProfileModule(), new InfraProfileModule(),
    ]));

    {
      const privateProfileFactory1 = container1.registry.resolve<PrivateProfileFactory>(PrivateProfileFactorySymbol);
      const profileA1Bis = await privateProfileFactory1.createFromReference(SimplePrivateProfileReference.from(profileAPrvRef1));
      const profileA1BisPrvRef = await profileA1Bis.getReference()
      expect(await profileAPrvRef1.serialize()).toEqual(await profileA1BisPrvRef.serialize());
    }
  });

});
