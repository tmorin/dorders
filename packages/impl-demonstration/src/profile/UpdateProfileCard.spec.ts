import {Container} from '@dorders/framework';
import {disposeContainers} from '@dorders/infra-test';
import {CreateProfile, ProfileCardUpdated, ProfileCreated, UpdateProfileCard} from '@dorders/model-profile';
import {startDemoContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('UpdateProfileCard', function () {

  let container0: Container;
  beforeEach(async function () {
    [container0] = await startDemoContainers(1);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should succeed', async function () {
    const [profileCreated] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));

    const updateProfileCard = new UpdateProfileCard({
      profileId: profileCreated.body.profileId,
      profileCard: 'profile0 bis'
    });
    const [profileCardUpdated] = await container0.messageBus.execute<ProfileCardUpdated>(updateProfileCard);
    expect(profileCardUpdated).toBeTruthy();
    expect(profileCreated.body.profileId).toEqual(updateProfileCard.body.profileId);
    expect(profileCardUpdated.body.profileId).toEqual(updateProfileCard.body.profileId);
    expect(profileCardUpdated.body.profileCard).toEqual(updateProfileCard.body.profileCard);
  });
});
