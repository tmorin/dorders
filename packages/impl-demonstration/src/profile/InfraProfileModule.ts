import {
  AbstractModule,
  ConfigProvider,
  ConfigProviderSymbol,
  LoggerFactory,
  LoggerFactorySymbol,
  MessageBus,
  MessageBusSymbol
} from '@dorders/framework';
import {JsonPublicProfileReferenceDeserializer} from './JsonPublicProfileReferenceDeserializer';
import {
  PrivateProfileFactory,
  PrivateProfileFactorySymbol,
  PrivateProfileReferenceDeserializer,
  PrivateProfileReferenceDeserializerSymbol,
  PrivateProfileRepository,
  PrivateProfileRepositorySymbol,
  ProfileSynchronizerService,
  ProfileSynchronizerServiceSymbol,
  PublicProfileReferenceDeserializer,
  PublicProfileReferenceDeserializerSymbol
} from '@dorders/model-profile';
import {JsonPrivateProfileReferenceDeserializer} from './JsonPrivateProfileReferenceDeserializer';
import {InMemoryPrivateProfileRepository} from './InMemoryPrivateProfileRepository';
import {SimplePrivateProfileFactory} from './SimplePrivateProfileFactory';
import {SimpleProfileSynchronizerService} from './SimpleProfileSynchronizerService';

export class InfraProfileModule extends AbstractModule {

  async configure(): Promise<void> {

    // SERVICES

    this.registry.registerFactory<PublicProfileReferenceDeserializer>(
      PublicProfileReferenceDeserializerSymbol,
      registry => new JsonPublicProfileReferenceDeserializer(),
      {
        singleton: true
      }
    );

    this.registry.registerFactory<PrivateProfileReferenceDeserializer>(
      PrivateProfileReferenceDeserializerSymbol,
      registry => new JsonPrivateProfileReferenceDeserializer(),
      {
        singleton: true
      }
    );

    this.registry.registerFactory<ProfileSynchronizerService>(
      ProfileSynchronizerServiceSymbol,
      registry => new SimpleProfileSynchronizerService(
        registry.resolve<MessageBus>(MessageBusSymbol),
        registry.resolve<LoggerFactory>(LoggerFactorySymbol)
      ),
      {
        singleton: true
      }
    );

    // FACTORIES

    this.registry.registerFactory<PrivateProfileFactory>(
      PrivateProfileFactorySymbol,
      registry => new SimplePrivateProfileFactory(
        registry.resolve<ConfigProvider>(ConfigProviderSymbol),
        registry.resolve<LoggerFactory>(LoggerFactorySymbol)
      ),
      {
        singleton: true
      }
    );

    // REPOSITORIES

    this.registry.registerFactory<PrivateProfileRepository>(
      PrivateProfileRepositorySymbol,
      registry => new InMemoryPrivateProfileRepository(
        registry.resolve<ConfigProvider>(ConfigProviderSymbol),
        registry.resolve<PrivateProfileReferenceDeserializer>(PrivateProfileReferenceDeserializerSymbol),
        registry.resolve<PrivateProfileFactory>(PrivateProfileFactorySymbol),
        registry.resolve<LoggerFactory>(LoggerFactorySymbol)
      ),
      {
        singleton: true
      }
    );

  }

  async dispose(): Promise<void> {
    await super.dispose();
    await this.registry.resolve<InMemoryPrivateProfileRepository>(PrivateProfileRepositorySymbol).dispose();
    await this.registry.resolve<SimplePrivateProfileFactory>(PrivateProfileFactorySymbol).dispose();
  }

}
