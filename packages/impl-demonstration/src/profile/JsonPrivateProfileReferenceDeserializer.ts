import {SimplePrivateProfileReference} from './SimplePrivateProfileReference';
import {
  PrivateProfileReference,
  PrivateProfileReferenceDeserializer,
  SerializedPrivateProfileReference
} from '@dorders/model-profile';


export class JsonPrivateProfileReferenceDeserializer implements PrivateProfileReferenceDeserializer {

  async deserialize(serializedPrivateProfileReference: SerializedPrivateProfileReference): Promise<PrivateProfileReference> {
    const {profileId, repositoryId} = JSON.parse(serializedPrivateProfileReference);
    return new SimplePrivateProfileReference(profileId, repositoryId);
  }

}
