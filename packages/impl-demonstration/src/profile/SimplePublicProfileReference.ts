import {ProfileId, PublicProfileReference, SerializedPublicProfileReference} from '@dorders/model-profile';

export class SimplePublicProfileReference implements PublicProfileReference {

  constructor(
    public readonly profileId: ProfileId,
    public readonly repositoryId: string,
    public readonly name: string
  ) {
  }

  static from(value: PublicProfileReference): SimplePublicProfileReference {
    if (value instanceof SimplePublicProfileReference) {
      return value;
    } else {
      throw new Error('unable to cast to SimplePublicProfileReference');
    }
  }

  async serialize(): Promise<SerializedPublicProfileReference> {
    return JSON.stringify(this);
  }

}
