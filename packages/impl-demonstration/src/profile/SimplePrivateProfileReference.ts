import {PrivateProfileReference, ProfileId, SerializedPrivateProfileReference} from '@dorders/model-profile';

export class SimplePrivateProfileReference implements PrivateProfileReference {

  constructor(
    public readonly profileId: ProfileId,
    public readonly repositoryId: string
  ) {
  }

  static from(value: PrivateProfileReference): SimplePrivateProfileReference {
    if (value instanceof SimplePrivateProfileReference) {
      return value;
    } else {
      throw new Error('unable to cast to SimplePrivateProfileReference');
    }
  }

  async serialize(): Promise<SerializedPrivateProfileReference> {
    return JSON.stringify(this);
  }

}
