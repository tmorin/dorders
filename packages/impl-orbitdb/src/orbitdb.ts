import Store from 'orbit-db-store';
import Logger from 'logplease';

const logger = Logger.create('orbitdb/waitForStore');

export async function waitForStore(store: Store, local = false): Promise<void> {
  const address = store.address.toString();

  const onLoadProgress = () => logger.debug('waitForStore', address, 'load.progress');
  store.events.on('load.progress', onLoadProgress);

  const onReady = () => logger.debug('waitForStore', address, 'ready');
  store.events.on('ready', onReady);

  const onReplicateProgress = () => logger.debug('waitForStore', address, 'replicate.progress');
  store.events.on('replicate.progress', onReplicateProgress);

  const onReplicated = () => logger.debug('waitForStore', address, 'replicated');
  store.events.on('replicated', onReplicated);

  try {
    if (local) {
      await store.load();
    } else {
      await new Promise(resolve => {
        const onReady = () => resolve()
        const onReplicated = () => store.load()
        store.events.once('ready', onReady);
        store.events.once('replicated', onReplicated);
      });
    }
  } finally {
    store.events.off('load.progress', onLoadProgress);
    store.events.off('ready', onReady);
    store.events.off('replicate.progress', onReplicateProgress);
    store.events.off('replicated', onReplicated);
  }
}
