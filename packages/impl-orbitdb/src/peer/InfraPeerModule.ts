import {LocalPeerFactorySymbol} from '@dorders/model-peer';
import {IpfsLocalPeerFactory} from './IpfsLocalPeerFactory';
import {AbstractModule} from '@dorders/framework';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';

export class InfraPeerModule extends AbstractModule {

  async configure(): Promise<void> {

    // FACTORIES

    this.registry.registerFactory(
      LocalPeerFactorySymbol,
      registry => new IpfsLocalPeerFactory(
        registry.resolve<IpfsService>(IpfsServiceSymbol)
      ),
      {
        singleton: true
      }
    );

  }

}
