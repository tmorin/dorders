import {LocalPeer, LocalPeerStarted, LocalPeerStopped, PeerId} from '@dorders/model-peer';
import {IpfsApi} from '@dorders/infra-ipfs';

export class IpfsLocalPeer implements LocalPeer {

  constructor(
    readonly peerId: PeerId,
    readonly ipfs: IpfsApi
  ) {
  }

  async applyPeerStarted(peerStarted: LocalPeerStarted): Promise<void> {
  }

  async applyPeerStopped(peerStopped: LocalPeerStopped): Promise<void> {
    await this.ipfs.stop();
  }

}
