import {Container} from '@dorders/framework';
import {disposeContainers} from '@dorders/infra-test';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {LocalPeerStarted, StartLocalPeer} from '@dorders/model-peer';
import {startOrbitdbContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('StartLocalPeer', function () {

  let container0: Container;
  beforeEach(async function () {
    [container0] = await startOrbitdbContainers(1);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should succeed', async function () {
    const command = new StartLocalPeer();
    const [localPeerStarted] = await container0.messageBus.execute<LocalPeerStarted>(command);
    expect(localPeerStarted).toBeTruthy();
    expect(localPeerStarted.body.peerId).toBeTruthy();
    const ipfs = await container0.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
    expect(ipfs.isOnline()).toBeTruthy();
  });

});
