import {Container} from '@dorders/framework';
import {disposeContainers} from '@dorders/infra-test';
import {LocalPeerStopped, StopLocalPeer} from '@dorders/model-peer';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {startOrbitdbContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('StopLocalPeer', function () {

  let container0: Container;
  beforeEach(async function () {
    [container0] = await startOrbitdbContainers(1);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should succeed', async function () {
    const command = new StopLocalPeer();
    const [localPeerStopped] = await container0.messageBus.execute<LocalPeerStopped>(command);
    expect(localPeerStopped).toBeTruthy();
    expect(localPeerStopped.body.peerId).toBeTruthy();
    const ipfs = await container0.registry.resolve<IpfsService>(IpfsServiceSymbol).get();
    expect(!ipfs.isOnline()).toBeTruthy();
  });

});
