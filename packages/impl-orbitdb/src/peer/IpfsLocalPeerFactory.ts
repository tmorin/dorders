import {LocalPeer, LocalPeerFactory} from '@dorders/model-peer';
import {IpfsService} from '@dorders/infra-ipfs';
import {IpfsLocalPeer} from './IpfsLocalPeer';

export class IpfsLocalPeerFactory implements LocalPeerFactory {

  constructor(
    private readonly ipfsService: IpfsService
  ) {
  }

  async create(): Promise<LocalPeer> {
    const ipfs = await this.ipfsService.get();
    const ipfsId = await ipfs.id();
    return new IpfsLocalPeer(ipfsId.id, ipfs);
  }

}
