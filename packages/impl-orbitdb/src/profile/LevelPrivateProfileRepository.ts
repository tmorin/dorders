import level, {Level} from 'level';
import {ProfileConfig, ProfileConfigScope} from './ProfileConfig';
import {OrbitdbPrivateProfileReference} from './OrbitdbPrivateProfileReference';
import {
  PrivateProfile,
  PrivateProfileFactory,
  PrivateProfileReference,
  PrivateProfileReferenceDeserializer,
  PrivateProfileRepository,
  ProfileId
} from '@dorders/model-profile';
import {ConfigProvider, createDirectory, Logger, LoggerFactory} from '@dorders/framework';

export class LevelPrivateProfileRepository implements PrivateProfileRepository {

  private readonly database: Level;

  private readonly logger: Logger = this.loggerFactory.create(LevelPrivateProfileRepository.name);

  private readonly map: Map<ProfileId, PrivateProfileReference>;

  constructor(
    private readonly configProvider: ConfigProvider,
    private readonly privateProfileReferenceDeserializer: PrivateProfileReferenceDeserializer,
    private readonly privateProfileFactory: PrivateProfileFactory,
    private readonly loggerFactory: LoggerFactory
  ) {
    const config: ProfileConfig = configProvider.get(ProfileConfigScope)
    this.logger.debug('create LevelPrivateProfileRepository in (%s)', config.repository);
    this.map = new Map();
    this.database = level(createDirectory(config.repository));
  }

  async add(privateProfile: PrivateProfile): Promise<void> {
    this.logger.debug('add the private profile (%s)', privateProfile.profileId);
    const privateProfileReference = await privateProfile.getReference();
    const reference = OrbitdbPrivateProfileReference.from(privateProfileReference).toLocal();
    const profileId = reference.profileId;
    return this.database.get(profileId).then(
      async () => {
        throw new Error(`profile (${profileId}) already managed`);
      },
      async () => {
        this.map.set(profileId, reference);
        const serializedValue = await reference.serialize();
        await this.database.put(profileId, serializedValue);
      }
    );
  }

  async get(profileId: ProfileId): Promise<PrivateProfile> {
    let privateProfileReference;
    if (this.map.has(profileId)) {
      privateProfileReference = this.map.get(profileId);
    } else {
      const serializedPrivateProfileReference = await this.database.get<string>(profileId);
      this.logger.debug('get (%s)', serializedPrivateProfileReference);
      privateProfileReference = await this.privateProfileReferenceDeserializer.deserialize(serializedPrivateProfileReference);
      this.map.set(profileId, privateProfileReference);
    }
    return await this.privateProfileFactory.createFromReference(privateProfileReference);
  }

  iterate(): AsyncIterable<PrivateProfile> {
    const stream = this.database.createKeyStream();
    const iterator = stream[Symbol.asyncIterator]();
    const next = async () => {
      const result = await iterator.next();
      if (result.done) {
        return {done: true, value: null};
      }
      const profile = await this.get(result.value);
      return {done: false, value: profile};
    };
    return {
      [Symbol.asyncIterator]: () => ({next})
    }
  }

  async list(): Promise<Array<PrivateProfile>> {
    return new Promise((resolve, reject) => {
      const privateProfiles: Array<PrivateProfile> = [];
      const stream = this.database.createKeyStream<ProfileId>();
      stream
        .on('data', async (profileId: ProfileId) => {
          try {
            stream.pause();
            const privateProfile = await this.get(profileId);
            privateProfiles.push(privateProfile);
            stream.resume();
          } catch (e) {
            stream.destroy(e);
          }
        })
        .on('error', reject)
        .on('end', () => resolve(privateProfiles))
    });
  }

  async remove(privateProfile: PrivateProfile): Promise<void> {
    try {
      await this.database.del(privateProfile.profileId);
    } finally {
      this.map.delete(privateProfile.profileId);
    }
  }

  async dispose(): Promise<void> {
    this.logger.info('dispose the profile repository')
    try {
      await this.database.close();
    } finally {
      this.map.clear();
    }
  }

}
