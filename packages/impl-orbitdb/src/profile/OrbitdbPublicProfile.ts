import {OrbitdbPublicProfileReference} from './OrbitdbPublicProfileReference';
import KeyValueStore from 'orbit-db-kvstore';
import {ProfileProperty} from './ProfileProperty';
import {ProfileCard, ProfileId, PublicProfile, PublicProfileReference} from '@dorders/model-profile';
import {valueAsString} from '@dorders/framework';

export class OrbitdbPublicProfile implements PublicProfile {

  constructor(
    public readonly profileId: ProfileId,
    public readonly publicStore: KeyValueStore
  ) {
  }

  get card(): ProfileCard {
    try {
      const profileCardAsString = this.publicStore.get<string>(ProfileProperty.profile_card);
      if (profileCardAsString && profileCardAsString.startsWith('{') && profileCardAsString.startsWith('[')) {
        return JSON.parse(profileCardAsString);
      }
      return profileCardAsString;
    } catch (e) {
      console.trace('unable to get the card for profile (%s)', this.profileId, e);
      return this.profileId;
    }
  }

  get name(): string {
    const card = this.card;
    if (!card) {
      return this.profileId;
    }
    if (typeof card === 'string') {
      return card;
    }
    if (card.name) {
      return valueAsString(card.name)
    }
    return valueAsString(card.alternateName) || this.profileId;
  }

  async getReference(): Promise<PublicProfileReference> {
    const profileId = this.profileId;
    const storeAddress = this.publicStore.address.toString();
    return new OrbitdbPublicProfileReference(profileId, storeAddress, this.name);
  }

}
