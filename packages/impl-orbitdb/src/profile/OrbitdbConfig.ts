import {OrbitDBOptions} from 'orbit-db';

export const OrbitdbConfigScope = Symbol.for('orbitdb')

export interface OrbitdbConfig {
  repository: string
  options?: OrbitDBOptions
}
