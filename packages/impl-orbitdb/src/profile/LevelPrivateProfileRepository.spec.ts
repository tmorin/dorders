import {Container} from '@dorders/framework';
import {disposeContainers} from '@dorders/infra-test';
import {
  PrivateProfileFactory,
  PrivateProfileFactorySymbol,
  PrivateProfileRepository,
  PrivateProfileRepositorySymbol
} from '@dorders/model-profile';
import {startOrbitdbContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('LevelPrivateProfileRepository', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers(50, 50, 50);
  });

  it('should iterate or list', async function () {
    const privateProfileFactory0 = await container0.registry.resolve<PrivateProfileFactory>(PrivateProfileFactorySymbol);
    const privateProfileRepository0 = await container0.registry.resolve<PrivateProfileRepository>(PrivateProfileRepositorySymbol);

    const privateProfileA = await privateProfileFactory0.createFromScratch();
    await privateProfileRepository0.add(privateProfileA);

    const privateProfileB = await privateProfileFactory0.createFromScratch();
    await privateProfileRepository0.add(privateProfileB);

    const privateProfileC = await privateProfileFactory0.createFromScratch();
    await privateProfileRepository0.add(privateProfileC);

    const profileIterator = privateProfileRepository0.iterate();
    let cnt = 0;
    for await (const profile of profileIterator) {
      cnt += 1;
    }
    expect(cnt).toEqual(3);

    const profileList = await privateProfileRepository0.list();
    expect(profileList.length).toEqual(3);
  });

});
