import {OrbitdbPublicProfileReference} from './OrbitdbPublicProfileReference';
import {
  PublicProfileReference,
  PublicProfileReferenceDeserializer,
  SerializedPublicProfileReference
} from '@dorders/model-profile';

export class JsonPublicProfileReferenceDeserializer implements PublicProfileReferenceDeserializer {

  async deserialize(serializedPublicProfileReference: SerializedPublicProfileReference): Promise<PublicProfileReference> {
    const {profileId, storeAddress, name, local = false} = JSON.parse(serializedPublicProfileReference);
    return new OrbitdbPublicProfileReference(profileId, storeAddress, name, local);
  }

}
