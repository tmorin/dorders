import OrbitDB from 'orbit-db';
import Identity from 'orbit-db-identity-provider/src/identity';
import KeyValueStore from 'orbit-db-kvstore';
import {EventEmitter} from 'events';
import {deleteDirectory, Logger} from '@dorders/framework';
import {
  PrivateProfile,
  PrivateProfileReference,
  ProfileCardUpdated,
  ProfileCreated,
  ProfileDeleted,
  ProfileId,
  ProfileSynchronized
} from '@dorders/model-profile';
import {OrbitdbPrivateProfileReference} from './OrbitdbPrivateProfileReference';
import {ProfileProperty} from './ProfileProperty';
import {OrbitdbPublicProfile} from './OrbitdbPublicProfile';

export class OrbitdbPrivateProfile extends EventEmitter implements PrivateProfile {

  static readonly EVENT_CLOSED = Symbol.for('closed');

  constructor(
    public readonly profileId: ProfileId,
    public readonly identity: Identity,
    public readonly privateStore: KeyValueStore,
    public readonly publicStore: KeyValueStore,
    public readonly orbitdb: OrbitDB,
    public readonly directory: string,
    public readonly logger: Logger,
    public readonly publicProfile = new OrbitdbPublicProfile(profileId, publicStore)
  ) {
    super();
  }

  static from(value: PrivateProfile): OrbitdbPrivateProfile {
    if (value instanceof OrbitdbPrivateProfile) {
      return value;
    } else {
      throw new Error('unable to cast to OrbitdbPrivateProfile');
    }
  }

  async getReference(): Promise<PrivateProfileReference> {
    const profileId = this.profileId;
    const storeAddress = this.privateStore.address.toString();
    const key = await this.identity.provider.keystore.getKey(profileId);
    const publicKey = key.public.marshal().toString('hex');
    const privateKey = key.marshal().toString('hex');
    return new OrbitdbPrivateProfileReference(profileId, privateKey, publicKey, storeAddress);
  }

  // profile

  async applyProfileCreated(profileCreated: ProfileCreated): Promise<void> {
    this.logger.debug('the profile (%s) has been created', this.profileId);
  }

  async applyProfileCardUpdated(profileCardUpdated: ProfileCardUpdated): Promise<void> {
    this.logger.debug('the card of the profile (%s) has been updated', this.profileId);
    const profileCard = profileCardUpdated.body.profileCard;
    const profileCardAsString = typeof profileCard === 'string' ? profileCard : JSON.stringify(profileCard);
    await this.publicStore.set(ProfileProperty.profile_card, profileCardAsString);
  }

  async applyProfilesSynchronized(profilesSynchronized: ProfileSynchronized): Promise<void> {
    this.logger.debug('the profile (%s) has been synchronized', this.profileId);
  }

  async applyProfileDeleted(deleted: ProfileDeleted): Promise<void> {
    this.logger.debug('the profile (%s) has been deleted', this.profileId);
    await this.destroy();
  }

  async stop() {
    try {
      await this.orbitdb.stop();
    } catch (e) {
      this.logger.warn(`unable to stop profile (${this.profileId})`, e);
    } finally {
      this.emit(OrbitdbPrivateProfile.EVENT_CLOSED);
      this.removeAllListeners();
    }
  }

  async destroy() {
    await this.stop();
    try {
      deleteDirectory(this.directory);
    } catch (e) {
      this.logger.warn(`unable to destroy profile (${this.profileId})`, e);
    }
  }

}
