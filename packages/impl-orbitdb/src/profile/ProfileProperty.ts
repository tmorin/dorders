export enum ProfileProperty {
  creation_date = 'creation_date',
  profile_card = 'profile_card',
  public_store_address = 'publicStoreAddress',
}
