import {OrbitdbPrivateProfileReference} from './OrbitdbPrivateProfileReference';
import {
  PrivateProfileReference,
  PrivateProfileReferenceDeserializer,
  SerializedPrivateProfileReference
} from '@dorders/model-profile';


export class JsonPrivateProfileReferenceDeserializer implements PrivateProfileReferenceDeserializer {

  async deserialize(serializedPrivateProfileReference: SerializedPrivateProfileReference): Promise<PrivateProfileReference> {
    const {profileId, privateKey, publicKey, storeAddress, local = false} = JSON.parse(serializedPrivateProfileReference);
    return new OrbitdbPrivateProfileReference(profileId, privateKey, publicKey, storeAddress, local);
  }

}
