import {Container} from '@dorders/framework';
import {disposeContainers, waitForOnce} from '@dorders/infra-test';
import {CreateProfile, ProfileCreated, ProfilesLoaded} from '@dorders/model-profile';
import {LocalPeerStarted} from '@dorders/model-peer';
import {startOrbitdbContainers} from '../__test__/container';

jest.setTimeout(15000);

describe('ProfilesLoader', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should load profiles', async function () {
    await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));
    await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileB'}));

    let cntProfileCreated = 0;
    container0.messageBus.on(ProfileCreated.EVENT_NAME, () => cntProfileCreated += 1);

    const pWaitForEvents = waitForOnce(container0, ProfilesLoaded.EVENT_NAME);

    await container0.messageBus.publish(new LocalPeerStarted({peerId: 'an identifier'}));
    await pWaitForEvents;

    expect(cntProfileCreated).toEqual(2);
  });

});
