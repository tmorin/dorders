import {OrbitdbPrivateProfile} from './OrbitdbPrivateProfile';
import {LoggerFactory, MessageBus} from '@dorders/framework';
import {PrivateProfile, ProfileSynchronized, ProfileSynchronizerService} from '@dorders/model-profile';

export class OrbitdbProfileSynchronizerService implements ProfileSynchronizerService {

  private readonly logger = this.loggerFactory.create('profile/OrbitdbProfileSynchronizer');

  constructor(
    private readonly messageBus: MessageBus,
    private readonly loggerFactory: LoggerFactory
  ) {
  }

  async startOngoingSynchronization(profile: PrivateProfile): Promise<void> {
    this.logger.debug('start ongoing synchronization for profile (%s)', profile.profileId);
    const orbitdbPrivateProfile = OrbitdbPrivateProfile.from(profile);
    for (const store of [orbitdbPrivateProfile.privateStore, orbitdbPrivateProfile.publicStore]) {
      store.events.on('replicated', () => {
        store.load();
      });
      store.events.on('ready', () => {
        this.messageBus.publish(new ProfileSynchronized({
          profileId: profile.profileId
        }));
      });
    }
  }

}
