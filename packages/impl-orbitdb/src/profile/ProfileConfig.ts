export const ProfileConfigScope = Symbol.for('profile')

export interface ProfileConfig {
  repository: string
}
