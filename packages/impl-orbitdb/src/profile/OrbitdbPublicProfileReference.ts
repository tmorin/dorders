import {ProfileId, PublicProfileReference, SerializedPublicProfileReference} from '@dorders/model-profile';

export class OrbitdbPublicProfileReference implements PublicProfileReference {

  constructor(
    public readonly profileId: ProfileId,
    public readonly storeAddress: string,
    public readonly name: string,
    public readonly local = false
  ) {
  }

  static from(value: PublicProfileReference): OrbitdbPublicProfileReference {
    if (value instanceof OrbitdbPublicProfileReference) {
      return value;
    } else {
      throw new Error('unable to cast to OrbitdbPublicProfileReference');
    }
  }

  async serialize(): Promise<SerializedPublicProfileReference> {
    return JSON.stringify(this);
  }

}
