import {PrivateProfileReference, ProfileId, SerializedPrivateProfileReference} from '@dorders/model-profile';

export class OrbitdbPrivateProfileReference implements PrivateProfileReference {

  constructor(
    public readonly profileId: ProfileId,
    public readonly privateKey: string,
    public readonly publicKey: string,
    public readonly storeAddress: string,
    public readonly local: boolean = false
  ) {
  }

  static from(value: PrivateProfileReference): OrbitdbPrivateProfileReference {
    if (value instanceof OrbitdbPrivateProfileReference) {
      return value;
    } else {
      throw new Error('unable to cast to OrbitdbPrivateProfileReference');
    }
  }

  toLocal(): OrbitdbPrivateProfileReference {
    return new OrbitdbPrivateProfileReference(
      this.profileId,
      this.privateKey,
      this.publicKey,
      this.storeAddress,
      true
    )
  }

  async serialize(): Promise<SerializedPrivateProfileReference> {
    return JSON.stringify(this);
  }

}
