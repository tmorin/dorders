import level from 'level';
import path from 'path';
import * as uuid from 'uuid';
import OrbitDB from 'orbit-db';
import Identities from 'orbit-db-identity-provider';
import Keystore from 'orbit-db-keystore';
import KeyValueStore from 'orbit-db-kvstore';
import {OrbitdbConfig, OrbitdbConfigScope} from './OrbitdbConfig';
import {OrbitdbPrivateProfile} from './OrbitdbPrivateProfile';
import {OrbitdbPrivateProfileReference} from './OrbitdbPrivateProfileReference';
import {ProfileProperty} from './ProfileProperty';
import {ConfigProvider, createDirectory, LoggerFactory} from '@dorders/framework';
import {PrivateProfile, PrivateProfileFactory, PrivateProfileReference, ProfileId} from '@dorders/model-profile';
import {IpfsService} from '@dorders/infra-ipfs';
import {waitForStore} from '../orbitdb';

export async function createKeystore(path: string, reference?: OrbitdbPrivateProfileReference): Promise<Keystore> {
  const database = level(createDirectory(path));
  if (reference) {
    await database.get(reference.profileId).then(
      null,
      () => database.put(reference.profileId, JSON.stringify({
        privateKey: reference.privateKey,
        publicKey: reference.publicKey
      }))
    );
  }

  return new Keystore(database);
}

export class OrbitdbPrivateProfileFactory implements PrivateProfileFactory {

  private readonly logger = this.loggerFactory.create(`profile/${OrbitdbPrivateProfileFactory.name}`);

  constructor(
    private readonly configProvider: ConfigProvider,
    private readonly loggerFactory: LoggerFactory,
    private readonly ipfsService: IpfsService,
    private readonly cache: Map<ProfileId, OrbitdbPrivateProfile> = new Map()
  ) {
  }

  async createFromScratch(): Promise<PrivateProfile> {
    const profileId: ProfileId = uuid.v4();
    this.logger.info('create profile (%s) from scratch', profileId);

    const ipfs = await this.ipfsService.get();
    const config: OrbitdbConfig = this.configProvider.get(OrbitdbConfigScope);
    const directory = path.join(config.repository, profileId);

    // create the key store
    const keystore = await createKeystore(path.join(directory, 'keystore'));

    // create the identity
    const identity = await Identities.createIdentity({id: profileId, keystore});

    // create the OrbitDB instance
    const orbitdb = await OrbitDB.createInstance(ipfs, {directory, keystore, identity});

    // create the public store
    const publicStore = await orbitdb.create<KeyValueStore>('publicProfile', 'keyvalue', {
      overwrite: false,
      replicate: true
    });
    await publicStore.set(ProfileProperty.creation_date, new Date().toISOString());

    // create the private store
    const privateStore = await orbitdb.create<KeyValueStore>('privateProfile', 'keyvalue', {
      overwrite: false,
      replicate: true
    });
    await privateStore.set(ProfileProperty.creation_date, new Date().toISOString());
    await privateStore.set(ProfileProperty.public_store_address, publicStore.address.toString());

    this.logger.info('profile (%s) created with identity (%s)', profileId, orbitdb.identity.id);

    return this.putToCache(new OrbitdbPrivateProfile(
      profileId,
      identity,
      privateStore,
      publicStore,
      orbitdb,
      directory,
      this.logger
    ));
  }

  async createFromReference(privateProfileReference: PrivateProfileReference): Promise<PrivateProfile> {
    const profileId = privateProfileReference.profileId;

    if (this.cache.has(profileId)) {
      return this.cache.get(profileId);
    }

    const reference = OrbitdbPrivateProfileReference.from(privateProfileReference);

    this.logger.info('create profile (%s) from (%s) reference', reference.profileId, reference.local ? 'local' : 'remote');

    const ipfs = await this.ipfsService.get();
    const config: OrbitdbConfig = this.configProvider.get(OrbitdbConfigScope);
    const directory = path.join(config.repository, profileId);

    // create the key store
    const keystore = await createKeystore(path.join(directory, 'keystore'), reference);

    // create the identity
    const identity = await Identities.createIdentity({id: profileId, keystore});

    // create the OrbitDB instance
    const orbitdb = await OrbitDB.createInstance(ipfs, {directory, keystore, identity});

    // open private store
    this.logger.debug('open private store for profile (%s)', reference.profileId);
    const privateStore = await orbitdb.open<KeyValueStore>(reference.storeAddress, {
      create: false,
      type: 'keyvalue',
      localOnly: false,
      overwrite: false,
      replicate: true
    });
    await waitForStore(privateStore, reference.local);

    this.logger.debug('private store for profile (%s) replicated', privateProfileReference.profileId);

    // open private store
    const publicStoreAddress = await privateStore.get<string>(ProfileProperty.public_store_address);
    const publicStore = await orbitdb.open<KeyValueStore>(publicStoreAddress, {
      create: false,
      type: 'keyvalue',
      localOnly: false,
      overwrite: false,
      replicate: true
    });
    await waitForStore(publicStore, reference.local);

    this.logger.info('profile (%s) created with identity (%s)', profileId, orbitdb.identity.id);

    return this.putToCache(new OrbitdbPrivateProfile(
      profileId,
      identity,
      privateStore,
      publicStore,
      orbitdb,
      directory,
      this.logger
    ));
  }

  async dispose() {
    this.logger.info('dispose the profile cache')
    for (const orbitdbPrivateProfile of this.cache.values()) {
      try {
        await orbitdbPrivateProfile.stop();
      } catch (e) {
        this.logger.debug('unable to close an OrbitDB instance', e.message);
      }
    }
    this.cache.clear();
  }

  private putToCache(orbitdbPrivateProfile: OrbitdbPrivateProfile) {
    this.cache.set(orbitdbPrivateProfile.profileId, orbitdbPrivateProfile);
    return orbitdbPrivateProfile.once(
      OrbitdbPrivateProfile.EVENT_CLOSED,
      () => this.cache.delete(orbitdbPrivateProfile.profileId)
    );
  }

}
