import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {disposeContainers, waitForOnce} from '@dorders/infra-test';
import {getSerializedPublicReference} from '../__test__/helpers';
import {CreateProfile, ProfileCreated} from '@dorders/model-profile';
import {
  AddContact,
  Contact,
  ContactDeleted,
  ContactRepository,
  ContactRepositorySymbol,
  RemoveContact
} from '@dorders/model-contact';

jest.setTimeout(15000);

describe('RemoveContact', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should succeed', async function () {
    const [profileACreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));

    for (let i = 0; i < 3; i++) {
      const [profileCreated1] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profile' + i}));
      const profilePubRef = await getSerializedPublicReference(container1, profileCreated1.body.profileId);
      await container0.messageBus.execute(new AddContact({
        profileId: profileACreated0.body.profileId,
        serializedReference: profilePubRef
      }))
    }

    const contactRepository = await container0.registry.resolve<ContactRepository>(ContactRepositorySymbol);
    const contactsBefore = contactRepository.iterate(profileACreated0.body.profileId);
    const contactsIteratorBefore: AsyncIterator<Contact> = contactsBefore[Symbol.asyncIterator]();
    expect((await contactsIteratorBefore.next()).done).toBeFalsy();
    const secondResult: IteratorResult<Contact, Contact> = await contactsIteratorBefore.next();
    expect((await contactsIteratorBefore.next()).done).toBeFalsy();
    expect((await contactsIteratorBefore.next()).done).toBeTruthy();

    const pWaitForContactDeleted = waitForOnce(container0, ContactDeleted.EVENT_NAME);

    const [contactRemoved] = await container0.messageBus.execute<ContactDeleted>(new RemoveContact({
      profileId: profileACreated0.body.profileId,
      contactId: secondResult.value.contactId
    }));

    expect(contactRemoved).toBeTruthy();
    expect(profileACreated0.body.profileId).toBe(contactRemoved.body.profileId);
    expect(secondResult.value.contactId).toBe(contactRemoved.body.contactId);

    await pWaitForContactDeleted;

    const contactsAfter = contactRepository.iterate(profileACreated0.body.profileId);
    const contactsIteratorAfter: AsyncIterator<Contact> = contactsAfter[Symbol.asyncIterator]();
    expect((await contactsIteratorAfter.next()).done).toBeFalsy();
    expect((await contactsIteratorAfter.next()).done).toBeFalsy();
    expect((await contactsIteratorAfter.next()).done).toBeTruthy();
  });

});
