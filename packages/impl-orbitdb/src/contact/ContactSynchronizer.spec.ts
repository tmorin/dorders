import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {disposeContainers, waitForOnce} from '@dorders/infra-test';
import {CreateProfile, ProfileCreated, UpdateProfileCard} from '@dorders/model-profile';
import {getSerializedPublicReference} from '../__test__/helpers';
import {AddContact, ContactRepository, ContactRepositorySymbol, ContactSynchronized} from '@dorders/model-contact';

jest.setTimeout(15000);

describe('ContactSynchronizer', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should start the ongoing synchronization ', async function () {
    const [profileACreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));

    const [profileBCreated1] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileB'}));
    const profileBPubRefAsString = await getSerializedPublicReference(container1, profileBCreated1.body.profileId);

    let waitForContactSynchronized = waitForOnce(container0, ContactSynchronized.EVENT_NAME);
    await container0.messageBus.execute(new AddContact({
      profileId: profileACreated0.body.profileId,
      serializedReference: profileBPubRefAsString
    }));
    await waitForContactSynchronized;

    waitForContactSynchronized = waitForOnce(container0, ContactSynchronized.EVENT_NAME);
    await container1.messageBus.execute(new UpdateProfileCard({
      profileId: profileBCreated1.body.profileId,
      profileCard: 'name bis'
    }));
    await waitForContactSynchronized;

    const contact = await container0.registry.resolve<ContactRepository>(ContactRepositorySymbol).get(
      profileACreated0.body.profileId,
      profileBCreated1.body.profileId
    );
    expect(contact.publicProfile.card).toBe('name bis');
  });

});
