import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {disposeContainers, waitFor} from '@dorders/infra-test';
import {ContactFactory, ContactFactorySymbol, ContactRepository, ContactRepositorySymbol} from '@dorders/model-contact';
import {CreateProfile, ProfileCreated} from '@dorders/model-profile';
import {getPublicReference} from '../__test__/helpers';

jest.setTimeout(15000);

describe('OrbitdbContactRepository', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should get and iterate', async function () {
    const contactFactory1 = container1.registry.resolve<ContactFactory>(ContactFactorySymbol);
    const contactRepository1 = container1.registry.resolve<ContactRepository>(ContactRepositorySymbol);

    const [profileCreated1] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profile'}));
    await waitFor(100);

    for (let i = 0; i < 3; i++) {
      const [profileCreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profile' + i}));
      const profilePubRef = await getPublicReference(container0, profileCreated0.body.profileId);
      const contact = await contactFactory1.createFromReference(profileCreated1.body.profileId, profilePubRef);
      await waitFor(100);
      await contactRepository1.persist(contact);
    }

    const contacts = contactRepository1.iterate(profileCreated1.body.profileId);
    let cnt = 0;
    for await (const contact of contacts) {
      cnt += 1;
      await expect(contactRepository1.get(contact.profileId, contact.contactId)).resolves.toBeTruthy();
    }
    expect(cnt).toEqual(3);
  });

});
