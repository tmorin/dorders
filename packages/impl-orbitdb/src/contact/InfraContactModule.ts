import {AbstractModule, LoggerFactory, LoggerFactorySymbol, MessageBus, MessageBusSymbol} from '@dorders/framework';
import {
  ContactFactory,
  ContactFactorySymbol,
  ContactRepository,
  ContactRepositorySymbol,
  ContactSynchronizationService,
  ContactSynchronizationServiceSymbol
} from '@dorders/model-contact';
import {OrbitdbContactFactory} from './OrbitdbContactFactory';
import {PrivateProfileRepository, PrivateProfileRepositorySymbol} from '@dorders/model-profile';
import {OrbitdbContactRepository} from './OrbitdbContactRepository';
import {OrbitdbContactSynchronizationService} from './OrbitdbContactSynchronizationService';

export class InfraContactModule extends AbstractModule {

  async configure(): Promise<void> {

    // FACTORIES

    this.registry.registerFactory<ContactFactory>(
      ContactFactorySymbol,
      registry => new OrbitdbContactFactory(
        registry.resolve<LoggerFactory>(LoggerFactorySymbol),
        registry.resolve<PrivateProfileRepository>(PrivateProfileRepositorySymbol)
      ),
      {
        singleton: true
      });

    // REPOSITORIES

    this.registry.registerFactory<ContactRepository>(
      ContactRepositorySymbol,
      registry => new OrbitdbContactRepository(
        registry.resolve<LoggerFactory>(LoggerFactorySymbol),
        registry.resolve<ContactFactory>(ContactFactorySymbol),
        registry.resolve<PrivateProfileRepository>(PrivateProfileRepositorySymbol)
      ),
      {
        singleton: true
      });

    // SERVICES

    this.registry.registerFactory<ContactSynchronizationService>(
      ContactSynchronizationServiceSymbol,
      registry => new OrbitdbContactSynchronizationService(
        registry.resolve<MessageBus>(MessageBusSymbol),
        registry.resolve<LoggerFactory>(LoggerFactorySymbol),
        registry.resolve<OrbitdbContactRepository>(ContactRepositorySymbol)
      ),
      {
        singleton: true
      });

  }

  async dispose(): Promise<void> {
    await this.registry.resolve<OrbitdbContactRepository>(ContactRepositorySymbol).dispose();
  }

}
