export const ContactConfigScope = Symbol.for('contact')

export interface ContactConfig {
  repository: string
}
