import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {disposeContainers, waitForOnce} from '@dorders/infra-test';
import {CreateProfile, DeleteProfile, ProfileCreated, ProfileDeleted} from '@dorders/model-profile';
import {getSerializedPublicReference} from '../__test__/helpers';
import {AddContact, ContactsCleared, ContactSynchronized} from '@dorders/model-contact';

jest.setTimeout(15000);

describe('ContactsClearer', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should clear contacts', async function () {
    const [profileCreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profile0'}));

    for (let i = 1; i < 2; i++) {
      // create the profile
      const [profileCreated1] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profile' + i}));
      const profilePubRefAsString = await getSerializedPublicReference(container1, profileCreated1.body.profileId);
      // create the contact
      let pWaitForContactSynchronized = waitForOnce(container0, ContactSynchronized.EVENT_NAME);
      await container0.messageBus.execute(new AddContact({
        profileId: profileCreated0.body.profileId,
        serializedReference: profilePubRefAsString
      }))
      await pWaitForContactSynchronized;
    }

    const pWaitForContactsDeleted = waitForOnce(container0, ContactsCleared.EVENT_NAME);
    const [profileDeleted0] = await container0.messageBus.execute<ProfileDeleted>(new DeleteProfile({
      profileId: profileCreated0.body.profileId
    }));
    expect(profileDeleted0).toBeTruthy();
    await pWaitForContactsDeleted;
  });

});

