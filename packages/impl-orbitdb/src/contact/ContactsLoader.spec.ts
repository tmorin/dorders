import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {disposeContainer, disposeContainers, waitFor, waitForMany, waitForOnce} from '@dorders/infra-test';
import {CreateProfile, ProfileCreated, ProfilesLoaded} from '@dorders/model-profile';
import {getSerializedPublicReference} from '../__test__/helpers';
import {AddContact, ContactCreated, ContactsLoaded} from '@dorders/model-contact';
import {StartLocalPeer} from '@dorders/model-peer';

jest.setTimeout(15000);

describe('ContactsLoader', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should load contacts', async function () {
    const [profileDCreated1] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileD'}));

    for (let i = 0; i < 3; i++) {
      const [profileCreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profile' + i}));
      const profilePubRefAsString = await getSerializedPublicReference(container0, profileCreated0.body.profileId);
      await container1.messageBus.execute(new AddContact({
        profileId: profileDCreated1.body.profileId,
        serializedReference: profilePubRefAsString
      }))
      await waitFor(200);
    }

    await disposeContainer(1);
    await waitFor(300);
    [container1] = await startOrbitdbContainers(1, {clean: false});

    {
      const pWaitForEvents = waitForOnce(container1, ProfilesLoaded.EVENT_NAME, ContactsLoaded.EVENT_NAME);
      const pWaitForContactCreated = waitForMany(container1, ContactCreated.EVENT_NAME, 3);
      await container1.messageBus.execute(new StartLocalPeer());
      await pWaitForEvents;
      await pWaitForContactCreated;
    }

    await waitFor(200);
  });

});
