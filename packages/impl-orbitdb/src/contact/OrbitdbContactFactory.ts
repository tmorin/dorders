import KeyValueStore from 'orbit-db-kvstore';
import {Contact, ContactFactory, ContactId} from '@dorders/model-contact';
import {PrivateProfileRepository, ProfileId, PublicProfileReference} from '@dorders/model-profile';
import {LoggerFactory} from '@dorders/framework';
import {OrbitdbPrivateProfile, OrbitdbPublicProfileReference} from '../profile';
import {waitForStore} from '../orbitdb';
import {ContactRecord} from './ContactRecord';
import {OrbitdbContact} from './OrbitdbContact';

export class OrbitdbContactFactory implements ContactFactory {

  private readonly logger = this.loggerFactory.create(`contact/${OrbitdbContactFactory.name}`);
  private readonly contactLogger = this.loggerFactory.create(`contact/${OrbitdbContact.name}`);

  constructor(
    private readonly loggerFactory: LoggerFactory,
    private readonly privateProfileRepository: PrivateProfileRepository
  ) {
  }

  async createFromReference(profileId: ProfileId, publicProfileReference: PublicProfileReference): Promise<Contact> {
    const contactId: ContactId = publicProfileReference.profileId;
    const contactName: string = publicProfileReference.name;

    this.logger.debug('create contact (%s/%s)', profileId, contactId);

    const orbitdbPublicProfileReference = OrbitdbPublicProfileReference.from(publicProfileReference);
    const privateProfile = await this.privateProfileRepository.get(profileId);
    const orbitdbPrivateProfile = OrbitdbPrivateProfile.from(privateProfile);

    // open store of the public profile
    const store = await orbitdbPrivateProfile.orbitdb.open<KeyValueStore>(orbitdbPublicProfileReference.storeAddress, {
      localOnly: false,
      create: false,
      type: 'keyvalue',
      overwrite: false,
      replicate: true
    });
    await waitForStore(store, orbitdbPublicProfileReference.local);

    // create the contact
    const publicStoreAddress = store.address.toString();
    const record: ContactRecord = {profileId, contactId, contactName, publicStoreAddress}
    const orbitdbContact = new OrbitdbContact(this.contactLogger, record, store);

    this.logger.debug('contact (%s/%s) created', profileId, contactId);

    return orbitdbContact;
  }

}
