import {ProfileId} from '@dorders/model-profile';
import {ContactId} from '@dorders/model-contact';

export interface ContactRecord {

  readonly profileId: ProfileId

  readonly contactId: ContactId

  contactName: string

  readonly publicStoreAddress: string

}
