import {ContactRecord} from './ContactRecord';
import {
  Contact,
  ContactCreated,
  ContactDeleted,
  ContactFactory,
  ContactId,
  ContactRepository
} from '@dorders/model-contact';
import {PrivateProfileRepository, ProfileId} from '@dorders/model-profile';
import {OrbitdbContact} from './OrbitdbContact';
import {OrbitdbPrivateProfile, OrbitdbPublicProfileReference} from '../profile';
import {LoggerFactory} from '@dorders/framework';

type ContactRecords = { [key: string]: ContactRecord };

export class OrbitdbContactRepository implements ContactRepository {

  public static PRIVATE_STORE_PROPERTY_CONTACTS = 'contacts';

  private readonly logger = this.loggerFactory.create(`contact/${OrbitdbContactRepository.name}`);

  constructor(
    private readonly loggerFactory: LoggerFactory,
    private readonly contactFactory: ContactFactory,
    private readonly privateProfileRepository: PrivateProfileRepository,
    private readonly cache: Map<ProfileId, Map<ContactId, OrbitdbContact>> = new Map()
  ) {
  }

  async persist(contact: Contact): Promise<void> {
    const profileId = contact.profileId;
    const contactId = contact.contactId;
    const orbitdbContact = OrbitdbContact.from(contact);

    const records = await this.loadRecords(profileId);
    records[contactId] = orbitdbContact.record;
    await this.storeRecords(profileId, records);
    await this.putToCache(profileId, orbitdbContact);
  }

  async get(profileId: ProfileId, contactId: ContactId): Promise<Contact> {
    if (this.cache.has(profileId) && this.cache.get(profileId).has(contactId)) {
      return this.cache.get(profileId).get(contactId);
    }

    const records = await this.loadRecords(profileId);
    const record = records[contactId];
    if (!record) {
      throw new Error(`unable to get the contact (${profileId}/${contactId})`);
    }

    return await this.createContact(profileId, record);
  }

  async delete(contact: Contact): Promise<void> {
    const profileId = contact.profileId;
    const contactId = contact.contactId;

    if (this.cache.has(profileId) && this.cache.get(profileId).has(contactId)) {
      this.cache.get(profileId).delete(contactId);
    }

    const records = await this.loadRecords(profileId);
    delete records[contactId];
    await this.storeRecords(profileId, records);
  }

  async clear(profileId: ProfileId): Promise<void> {
    if (this.cache.has(profileId)) {
      this.cache.delete(profileId);
    }
  }

  async cleanCachedContacts(profileId: ProfileId): Promise<{
    contactCreatedList: Array<ContactCreated>,
    contactDeletedList: Array<ContactDeleted>
  }> {
    const cachedContacts: Map<ContactId, OrbitdbContact> = this.cache.get(profileId) || new Map();
    const contactCreatedList: Array<ContactCreated> = [];
    const contactDeletedList: Array<ContactDeleted> = [];
    const existingContactIds: Array<ContactId> = [];

    // load current contacts
    const loadedContacts = this.iterate(profileId);
    for await (const contact of loadedContacts) {
      if (!cachedContacts.has(contact.contactId)) {
        // the contact has been created
        const contactCreated = new ContactCreated({profileId, contactId: contact.contactId});
        await contact.applyContactCreated(contactCreated);
        contactCreatedList.push(contactCreated);
      } else {
        existingContactIds.push(contact.contactId);
      }
    }

    // detect orphan contacts
    for (const [contactId, contact] of cachedContacts.entries()) {
      if (existingContactIds.indexOf(contactId) < 0) {
        // the contact is now orphan, it has to be deleted
        const contactDeleted = new ContactDeleted({profileId, contactId});
        await contact.applyContactDeleted(contactDeleted);
        await contact.destroy();
        contactDeletedList.push(contactDeleted);
      }
    }

    return {contactCreatedList, contactDeletedList};
  }

  iterate(profileId: ProfileId): AsyncIterable<Contact> {
    return {
      [Symbol.asyncIterator]: () => {
        const promise = this.loadRecordsAsArray(profileId);
        let index = 0;
        const next = async () => {
          const records = await promise;
          const record = records[index];
          index += 1;
          if (record) {
            const contact = await this.createContact(record.profileId, record);
            return {done: false, value: contact};
          }
          return {done: true, value: null};
        };
        return {next};
      }
    };
  }

  async dispose(): Promise<void> {
    this.logger.info('dispose the contact repository')
    for (const contactsByProfile of this.cache.values()) {
      for (const contact of contactsByProfile.values()) {
        await contact.stop();
      }
      contactsByProfile.clear();
    }
    this.logger.info('dispose the contact cache')
    this.cache.clear();
  }

  private async storeRecords(profileId: ProfileId, records: ContactRecords): Promise<void> {
    this.logger.debug('store (%s) contacts for profile (%s)', Object.keys(records).length, profileId);
    const profile = await this.privateProfileRepository.get(profileId);
    await OrbitdbPrivateProfile.from(profile).privateStore.set(
      OrbitdbContactRepository.PRIVATE_STORE_PROPERTY_CONTACTS,
      JSON.stringify(records)
    );
  }

  private async loadRecordsAsArray(profileId: ProfileId): Promise<Array<ContactRecord>> {
    return Object.values(await this.loadRecords(profileId));
  }

  private async loadRecords(profileId: ProfileId): Promise<ContactRecords> {
    this.logger.debug('load contacts for profile (%s)', profileId);
    try {
      const profile = await this.privateProfileRepository.get(profileId);
      const recordsAsJson = OrbitdbPrivateProfile.from(profile).privateStore.get<string>(
        OrbitdbContactRepository.PRIVATE_STORE_PROPERTY_CONTACTS
      );
      return JSON.parse(recordsAsJson);
    } catch (e) {
      this.logger.debug('no contact found for profile (%s) - %s', profileId, e.message);
      return {};
    }
  }

  private async createContact(profileId: ProfileId, record: ContactRecord): Promise<Contact> {
    // an instance of OrbitdbContact should only be created once by OrbitDB instance
    if (this.cache.has(profileId) && this.cache.get(profileId).has(record.contactId)) {
      return this.cache.get(profileId).get(record.contactId);
    }

    // create the contact instance from its reference
    const contact = await this.contactFactory.createFromReference(profileId, new OrbitdbPublicProfileReference(
      record.contactId,
      record.publicStoreAddress,
      record.contactName,
      true
    ));
    const orbitdbContact = OrbitdbContact.from(contact);

    return this.putToCache(profileId, orbitdbContact);
  }

  private async putToCache(profileId: ProfileId, orbitdbContact: OrbitdbContact) {
    if (!this.cache.has(profileId)) {
      this.cache.set(profileId, new Map());
    }
    if (!this.cache.get(profileId).has(orbitdbContact.contactId)) {
      this.cache.get(profileId).set(orbitdbContact.contactId, orbitdbContact);
      const profile = await this.privateProfileRepository.get(profileId);
      const orbitdbPrivateProfile = OrbitdbPrivateProfile.from(profile);
      orbitdbPrivateProfile.once(
        OrbitdbPrivateProfile.EVENT_CLOSED,
        () => this.cache.delete(orbitdbPrivateProfile.profileId)
      );
    }
    return orbitdbContact;
  }

}
