import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {disposeContainers, waitFor} from '@dorders/infra-test';
import {CreateProfile, ProfileCreated} from '@dorders/model-profile';
import {ContactFactory, ContactFactorySymbol} from '@dorders/model-contact';
import {OrbitdbContact} from './OrbitdbContact';
import {getPublicReference} from '../__test__/helpers';

jest.setTimeout(15000);

describe('OrbitdbContactFactory', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should create from reference', async function () {
    const [profileACreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));
    const [profileBCreated1] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileB'}));

    const profileAPubRef = await getPublicReference(container0, profileACreated0.body.profileId);

    const contactFactory1 = container1.registry.resolve<ContactFactory>(ContactFactorySymbol);
    await waitFor(100);
    const contact = await contactFactory1.createFromReference(profileBCreated1.body.profileId, profileAPubRef);
    await waitFor(100);

    expect(contact.contactId).toEqual(profileACreated0.body.profileId);
    expect(contact.publicProfile.profileId).toEqual(profileACreated0.body.profileId);
    expect(contact.name).toEqual('profileA');

    await OrbitdbContact.from(contact).destroy();
    await waitFor(300);
  });

});
  
