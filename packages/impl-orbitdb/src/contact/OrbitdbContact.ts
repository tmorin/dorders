import {Contact, ContactCreated, ContactDeleted, ContactId, ContactRenamed} from '@dorders/model-contact';
import {ContactRecord} from './ContactRecord';
import KeyValueStore from 'orbit-db-kvstore';
import {OrbitdbPublicProfile} from '../profile';
import {ProfileId} from '@dorders/model-profile';
import {Logger} from '@dorders/framework';

export class OrbitdbContact implements Contact {

  constructor(
    private readonly logger: Logger,
    public readonly record: ContactRecord,
    public readonly publicStore: KeyValueStore,
    public readonly publicProfile = new OrbitdbPublicProfile(record.contactId, publicStore)
  ) {
  }

  get profileId(): ProfileId {
    return this.record.profileId;
  }

  get contactId(): ContactId {
    return this.record.contactId;
  }

  get name(): string {
    return this.record.contactName;
  }

  static from(value: Contact): OrbitdbContact {
    if (value instanceof OrbitdbContact) {
      return value;
    } else {
      throw new Error('unable to cast to OrbitdbContact');
    }
  }

  async applyContactCreated(contactAdded: ContactCreated): Promise<void> {
  }

  async applyContactRenamed(contactRenamed: ContactRenamed): Promise<void> {
    this.record.contactName = contactRenamed.body.newName;
  }

  async applyContactDeleted(contactRemoved: ContactDeleted): Promise<void> {
    await this.destroy();
  }

  async stop() {
    try {
      await this.publicStore.close();
    } catch (e) {
      this.logger.warn(`unable to close contact (${this.profileId}/${this.contactId}) `, e);
    }
  }

  async destroy() {
    await this.stop();
    try {
      await this.publicStore.drop();
    } catch (e) {
      this.logger.warn(`unable to destroy contact (${this.profileId}/${this.contactId}) `, e);
    }
  }

}
