import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {getSerializedPublicReference} from '../__test__/helpers';
import {disposeContainers, waitFor, waitForOnce} from '@dorders/infra-test';
import {CreateProfile, ProfileCreated} from '@dorders/model-profile';
import {
  AddContact,
  Contact,
  ContactCreated,
  ContactRenamed,
  ContactRepository,
  ContactRepositorySymbol,
  RenameContact
} from '@dorders/model-contact';

jest.setTimeout(15000);

describe('RenameContact', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should succeed', async function () {
    const [profileACreated0] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));
    const [profileBCreated1] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileB'}));
    const profileBPubRefAsString = await getSerializedPublicReference(container1, profileBCreated1.body.profileId);

    const [contactBAdded0] = await container0.messageBus.execute<ContactCreated>(new AddContact({
      profileId: profileACreated0.body.profileId,
      serializedReference: profileBPubRefAsString
    }));

    await waitFor(100);

    const pWaitForEvents = waitForOnce(container0, ContactRenamed.EVENT_NAME);

    const [contactBRenamed0] = await container0.messageBus.execute<ContactRenamed>(new RenameContact({
      profileId: profileACreated0.body.profileId,
      contactId: contactBAdded0.body.contactId,
      newName: 'profileB bis'
    }));

    expect(contactBRenamed0).toBeTruthy();
    expect(profileACreated0.body.profileId).toBe(contactBRenamed0.body.profileId);
    expect(profileBCreated1.body.profileId).toBe(contactBRenamed0.body.contactId);
    expect(contactBRenamed0.body.oldName).toBe('profileB');
    expect(contactBRenamed0.body.newName).toBe('profileB bis');

    await pWaitForEvents;

    const contactRepository = await container0.registry.resolve<ContactRepository>(ContactRepositorySymbol);
    const contacts = contactRepository.iterate(profileACreated0.body.profileId);
    const contactsIterator: AsyncIterator<Contact> = contacts[Symbol.asyncIterator]();
    const result: IteratorResult<Contact, Contact> = await contactsIterator.next();
    expect(result.done).toBeFalsy();
    expect(result.value.name).toBe('profileB bis');
    expect((await contactsIterator.next()).done).toBeTruthy();
  });

});
