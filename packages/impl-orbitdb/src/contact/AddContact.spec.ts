import {Container} from '@dorders/framework';
import {startOrbitdbContainers} from '../__test__/container';
import {disposeContainers, waitForOnce} from '@dorders/infra-test';
import {CreateProfile, ProfileCreated} from '@dorders/model-profile';
import {AddContact, ContactCreated, ContactRepository, ContactRepositorySymbol} from '@dorders/model-contact';
import {getSerializedPublicReference} from '../__test__/helpers';

jest.setTimeout(15000);

describe('AddContact', function () {

  let container0: Container;
  let container1: Container;
  beforeEach(async function () {
    [container0, container1] = await startOrbitdbContainers(2);
  });
  afterEach(async function () {
    await disposeContainers();
  });

  it('should succeed', async function () {
    const [profileACreated] = await container0.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileA'}));
    const profileAPubRefAsString = await getSerializedPublicReference(container0, profileACreated.body.profileId);


    let pWaitForEvents = waitForOnce(container1, ProfileCreated.EVENT_NAME);
    const [profileBCreated] = await container1.messageBus.execute<ProfileCreated>(new CreateProfile({profileCard: 'profileB'}));
    await pWaitForEvents;

    pWaitForEvents = waitForOnce(container1, ContactCreated.EVENT_NAME);
    const [contactAAdded] = await container1.messageBus.execute<ContactCreated>(new AddContact({
      profileId: profileBCreated.body.profileId,
      serializedReference: profileAPubRefAsString
    }));
    await pWaitForEvents;

    expect(contactAAdded).toBeTruthy();
    expect(profileBCreated.body.profileId).toEqual(contactAAdded.body.profileId);
    expect(profileACreated.body.profileId).toEqual(contactAAdded.body.contactId);

    const contactRepository = await container1.registry.resolve<ContactRepository>(ContactRepositorySymbol);
    const contacts = contactRepository.iterate(profileBCreated.body.profileId);
    const iterator = contacts[Symbol.asyncIterator]();
    expect((await iterator.next()).done).toBeFalsy();
    expect((await iterator.next()).done).toBeTruthy();
  });

});
