import {LoggerFactory, MessageBus} from '@dorders/framework';
import {ContactId, ContactSynchronizationService, ContactSynchronized} from '@dorders/model-contact';
import {OrbitdbContactRepository} from './OrbitdbContactRepository';
import {ProfileId} from '@dorders/model-profile';
import {OrbitdbContact} from './OrbitdbContact';

export class OrbitdbContactSynchronizationService implements ContactSynchronizationService {

  private readonly logger = this.loggerFactory.create(`contact/${OrbitdbContactSynchronizationService.name}`);

  constructor(
    private readonly messageBus: MessageBus,
    private readonly loggerFactory: LoggerFactory,
    private readonly contactRepository: OrbitdbContactRepository
  ) {
  }

  async check(profileId: ProfileId): Promise<void> {
    const result = await this.contactRepository.cleanCachedContacts(profileId);

    for (const contactDeleted of result.contactDeletedList) {
      await this.messageBus.publish(contactDeleted);
    }

    for (const contactCreated of result.contactCreatedList) {
      await this.messageBus.publish(contactCreated);
    }
  }

  async monitor(profileId: ProfileId, contactId: ContactId): Promise<void> {
    this.logger.debug('start ongoing synchronization for contact (%s/%s)', profileId, contactId);

    // get the OrbitdbContact
    const contact = await this.contactRepository.get(profileId, contactId);
    const orbitdbContact = OrbitdbContact.from(contact);

    // when data are replicated, data have to be loaded in memory 
    orbitdbContact.publicStore.events.on('replicated', () => {
      this.logger.debug('contact (%s/%s) replicated', profileId, contactId);
      orbitdbContact.publicStore.load();
    });

    // once data are loaded in memory, the contact is considered "synchronized"
    orbitdbContact.publicStore.events.on('ready', () => {
      this.logger.debug('contact (%s/%s) ready', profileId, contactId);
      this.messageBus.publish(new ContactSynchronized({
        profileId: contact.profileId,
        contactId: contact.contactId
      }));
    });
  }

}
