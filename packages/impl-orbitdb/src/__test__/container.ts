import {CreateOptions} from '@dorders/infra-test';
import {startIpfsContainers} from '@dorders/infra-test-ipfs';
import {LogpleaseLogger} from '@dorders/infra-logger-logplease';
import {ModelPeerModule} from '@dorders/model-peer';
import {ModelContactModule} from '@dorders/model-contact';
import {ModelProfileModule} from '@dorders/model-profile';
import {ImplOrbitdbTestModule} from './ImplOrbitdbTestModule';
import {InfraPeerModule} from '../peer';
import {InfraContactModule} from '../contact';
import {InfraProfileModule} from '../profile';

LogpleaseLogger.setLevel('WARN');

export async function startOrbitdbContainers(
  numbers: number,
  options: Partial<CreateOptions> = {}
) {
  return await startIpfsContainers(numbers, options, () => ([
    new ImplOrbitdbTestModule(),
    new ModelPeerModule(), new InfraPeerModule(),
    new ModelProfileModule(), new InfraProfileModule(),
    new ModelContactModule(), new InfraContactModule(),
  ]))
}
