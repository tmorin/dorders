import {ConfigsTestProvider} from '@dorders/infra-test';
import {Config, Logger, LoggerFactory} from '@dorders/framework';
import {OrbitdbConfigScope, ProfileConfigScope} from '../profile';
import {ContactConfigScope} from '../contact/ContactConfig';

export class ImplOrbitdbConfigsTestProvider extends ConfigsTestProvider {

  private readonly logger: Logger = this.loggerFactory.create(ImplOrbitdbConfigsTestProvider.name);

  constructor(
    private readonly loggerFactory: LoggerFactory
  ) {
    super();
  }

  async provide(index: number): Promise<Map<symbol, Config>> {
    const map = new Map();

    this.logger.debug('index (%s) - set test config for scope (%s)', index, OrbitdbConfigScope);
    map.set(OrbitdbConfigScope, {
      repository: `tmp/container-${index}/orbitdb`
    });

    this.logger.debug('index (%s) - set test config for scope (%s)', index, ProfileConfigScope);
    map.set(ProfileConfigScope, {
      repository: `tmp/container-${index}/profile`
    });

    this.logger.debug('index (%s) - set test config for scope (%s)', index, ContactConfigScope);
    map.set(ContactConfigScope, {
      repository: `tmp/container-${index}/contact`
    });

    return map;
  }

}
