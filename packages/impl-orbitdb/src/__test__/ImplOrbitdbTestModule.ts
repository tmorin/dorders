import {AbstractModule, LoggerFactory, LoggerFactorySymbol} from '@dorders/framework';
import {ConfigsTestProviderSymbol} from '@dorders/infra-test';
import {ImplOrbitdbConfigsTestProvider} from './ImplOrbitdbConfigsTestProvider';

export class ImplOrbitdbTestModule extends AbstractModule {

  async configure(): Promise<void> {

    this.registry.registerFactory(
      ConfigsTestProviderSymbol,
      registry => new ImplOrbitdbConfigsTestProvider(
        registry.resolve<LoggerFactory>(LoggerFactorySymbol)
      ),
      {
        singleton: true
      }
    );

  }

}
