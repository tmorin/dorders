import OrbitDB from 'orbit-db';
import KeyValueStore from 'orbit-db-kvstore';
import {Container} from '@dorders/framework';
import {disposeContainers, waitFor} from '@dorders/infra-test';
import {IpfsService, IpfsServiceSymbol} from '@dorders/infra-ipfs';
import {waitForStore} from './orbitdb';
import {startOrbitdbContainers} from './__test__/container';

jest.setTimeout(15000);

describe('orbitdb', function () {

  let container0: Container, container1: Container, container2: Container;
  let orbitdb0: OrbitDB, orbitdb1: OrbitDB, orbitdb2: OrbitDB;
  beforeEach(async () => {
    [container0, container1, container2] = await startOrbitdbContainers(3);
    orbitdb0 = await OrbitDB.createInstance(
      await container0.registry.resolve<IpfsService>(IpfsServiceSymbol).get(), {directory: 'tmp/container-0/orbitdb'}
    );
    orbitdb1 = await OrbitDB.createInstance(
      await container1.registry.resolve<IpfsService>(IpfsServiceSymbol).get(), {directory: 'tmp/container-1/orbitdb'}
    );
    orbitdb2 = await OrbitDB.createInstance(
      await container2.registry.resolve<IpfsService>(IpfsServiceSymbol).get(), {directory: 'tmp/container-2/orbitdb'}
    );
  });
  afterEach(async () => {
    for (const instance of [orbitdb0, orbitdb1, orbitdb2]) {
      await instance.stop();
      await waitFor(50);
    }
    await disposeContainers();
  });

  it('should wait for store', async function () {
    const sharedDb0 = await orbitdb0.create<KeyValueStore>('shared-database', 'keyvalue', {
      replicate: true,
      overwrite: false
    });
    await waitForStore(sharedDb0, true);
    await sharedDb0.set('key0', 'value0');

    const sharedDb0Bis = await orbitdb0.create<KeyValueStore>('shared-database-bis', 'keyvalue', {
      replicate: true,
      overwrite: false
    });
    await waitForStore(sharedDb0Bis, true);
    await sharedDb0Bis.set('key0bis', 'value0bis');

    await waitFor(200);

    const sharedDb1 = await orbitdb1.open<KeyValueStore>(sharedDb0.address.toString(), {
      localOnly: false,
      create: false,
      type: 'keyvalue',
      replicate: true,
      overwrite: false
    });
    await waitForStore(sharedDb1, false);

    await waitFor(200);

    const sharedDb2 = await orbitdb2.open<KeyValueStore>(sharedDb0.address.toString(), {
      localOnly: false,
      create: false,
      type: 'keyvalue',
      replicate: true,
      overwrite: false
    });
    await waitForStore(sharedDb2, false);

    const sharedDbBis2 = await orbitdb2.open<KeyValueStore>(sharedDb0Bis.address.toString(), {
      localOnly: false,
      create: false,
      type: 'keyvalue',
      replicate: true,
      overwrite: false
    });
    await waitForStore(sharedDbBis2, false);

    await waitFor(200);

    expect(sharedDb0.get('key0')).toEqual('value0');
    expect(sharedDb1.get('key0')).toEqual('value0');
    expect(sharedDb2.get('key0')).toEqual('value0');
    expect(sharedDbBis2.get('key0bis')).toEqual('value0bis');
  });

});
