import Logplease from 'logplease';
import {Logger, LoggerFactory} from '@dorders/framework';
import {LogpleaseLogger} from './LogpleaseLogger';

export class LogpleaseLoggerFactory implements LoggerFactory {

  create(...names: Array<string>): Logger {
    return new LogpleaseLogger(
      Logplease.create(names.join('/'))
    );
  }

}
