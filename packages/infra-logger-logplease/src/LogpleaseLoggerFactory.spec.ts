import {LogpleaseLoggerFactory} from '.';

jest.setTimeout(15000);

describe('LogpleaseLoggerFactory', function () {

  it('should resolve a logger factory', async function () {
    const loggerFactory = new LogpleaseLoggerFactory();
    const logger = loggerFactory.create('test');
    expect(logger).toBeTruthy();
  })

})
