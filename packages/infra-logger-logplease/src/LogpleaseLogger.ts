import Logplease from 'logplease';
import {Logger} from '@dorders/framework';

export class LogpleaseLogger implements Logger {

  constructor(
    public readonly logger: Logplease
  ) {
  }

  static setLevel(level: string) {
    Logplease.setLogLevel(level);
  }

  static setLogfile(filename: string) {
    Logplease.setLogfile(filename);
  }

  debug(message?: any, ...optionalParams: any[]): void {
    this.logger.debug.apply(this.logger, [message, ...optionalParams]);
  }

  log(message?: any, ...optionalParams: any[]): void {
    this.logger.log.apply(this.logger, [message, ...optionalParams]);
  }

  info(message?: any, ...optionalParams: any[]): void {
    this.logger.info.apply(this.logger, [message, ...optionalParams]);
  }

  warn(message?: any, ...optionalParams: any[]): void {
    this.logger.warn.apply(this.logger, [message, ...optionalParams]);
  }

  error(message?: any, ...optionalParams: any[]): void {
    this.logger.error.apply(this.logger, [message, ...optionalParams]);
  }

}
