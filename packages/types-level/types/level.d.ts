declare module 'level' {
  import {EventEmitter} from 'events';
  import {Readable} from 'readable-stream';

  export interface DefaultOptions {
    keyEncoding?: string
    valueEncoding?: string
  }

  export interface levelOptions extends DefaultOptions {
    createIfMissing?: boolean
  }

  export default function level(location: string, options?: levelOptions): Level

  export class Batch {
    length: number;

    put(key: string, value: any): Batch

    del(key: string): Batch

    clear(): Batch

    write(options?: DefaultOptions): Promise<void>
  }

  export interface QueryOptions {
    gt?: any
    lt?: any
    reverse?: boolean
    limit?: number
    keys?: boolean
    values?: boolean
  }

  export type DatabaseEvent = 'put' | 'del' | 'batch' | 'opening' | 'open' | 'ready' | 'closing' | 'closed';

  export class Level extends EventEmitter {
    open(): Promise<void>

    close(): Promise<void>

    put(key: string, value: any, options: DefaultOptions, callback: (error?: Error) => void): void
    put(key: string, value: any, options?: DefaultOptions): Promise<void>

    get<T>(key: string, options: DefaultOptions, callback: (error?: Error, value?: T) => void): void
    get<T>(key: string, options?: DefaultOptions): Promise<T>

    del(key: string, options: DefaultOptions, callback: (error?: Error) => void): void
    del(key: string, options?: DefaultOptions): Promise<void>

    batch(): Batch

    batch(array: Array<{ type: 'put' | 'del', key: string, value?: any }>, options: DefaultOptions, callback: (error?: Error) => void): void
    batch(array: Array<{ type: 'put' | 'del', key: string, value?: any }>, options?: DefaultOptions): Promise<void>

    isOpen(): boolean

    isClosed(): boolean

    createReadStream<K>(options?: QueryOptions): Readable
    createReadStream<K, V>(options?: QueryOptions): Readable

    createKeyStream<T>(options?: QueryOptions): Readable

    createValueStream<T>(options?: QueryOptions): Readable

    iterator<T>(options?: QueryOptions): Iterator<T>

    on(event: 'put', listener: (key: string, value: any) => void): this;

    once(event: 'put', listener: (key: string, value: any) => void): this;

    on(event: 'del', listener: (key: string) => void): this;

    once(event: 'del', listener: (key: string) => void): this;

    on(event: 'batch', listener: (operations: Array<any>) => void): this;

    once(event: 'batch', listener: (operations: Array<any>) => void): this;

    on(event: 'opening' | 'open' | 'ready' | 'closing' | 'closed', listener: () => void): this;

    once(event: 'opening' | 'open' | 'ready' | 'closing' | 'closed', listener: () => void): this;

    on(event: DatabaseEvent | symbol, listener: (...args: any[]) => void): this;

    once(event: DatabaseEvent | symbol, listener: (...args: any[]) => void): this;

    prependListener(event: DatabaseEvent | symbol, listener: (...args: any[]) => void): this;

    prependOnceListener(event: DatabaseEvent | symbol, listener: (...args: any[]) => void): this;

    removeListener(event: DatabaseEvent | symbol, listener: (...args: any[]) => void): this;

    off(event: DatabaseEvent | symbol, listener: (...args: any[]) => void): this;

    listeners(event: DatabaseEvent | symbol): Function[];

    rawListeners(event: DatabaseEvent | symbol): Function[];

    emit(event: DatabaseEvent | symbol, ...args: any[]): boolean;

  }

}
