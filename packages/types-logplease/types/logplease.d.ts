declare module 'logplease' {

  export type LogLevel = 'DEBUG' | 'INFO' | 'WARN' | 'ERROR' | 'NONE' | string;

  const LogLevels: {
    'DEBUG': LogLevel
    'INFO': LogLevel
    'WARN': LogLevel
    'ERROR': LogLevel
    'NONE': LogLevel
  };

  type Color = number | string

  const Colors: {
    'Black': Color
    'Red': Color
    'Green': Color
    'Yellow': Color
    'Blue': Color
    'Magenta': Color
    'Cyan': Color
    'Grey': Color
    'White': Color
    'Default': Color
  };

  export interface CreateLoggerOptions {
    useColors?: string
    color?: Color
    showTimestamp?: boolean
    useLocalTime?: boolean
    showLevel?: boolean
    filename?: string
    appendFile?: boolean
  }

  export default class Logger {
    static readonly Colors: typeof Colors;
    static readonly LogLevels: typeof LogLevels;

    constructor(category, options)

    static setLogLevel(level: LogLevel)

    static setLogfile(filename: string)

    static create(name: string, options?: CreateLoggerOptions): Logger

    debug(message?: any, ...optionalParams: any[]): void

    log(message?: any, ...optionalParams: any[]): void

    info(message?: any, ...optionalParams: any[]): void

    warn(message?: any, ...optionalParams: any[]): void

    error(message?: any, ...optionalParams: any[]): void
  }
}
