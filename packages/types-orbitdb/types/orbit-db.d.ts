declare module 'orbit-db' {
  import {IpfsApi} from 'ipfs';
  import Keystore from 'orbit-db-keystore';
  import Identity from 'orbit-db-identity-provider/src/identity';
  import Store, {StoreType} from 'orbit-db-store';
  import KeyValueStore from 'orbit-db-kvstore';
  import EventStore from 'orbit-db-eventstore';
  import FeedStore from 'orbit-db-feedstore';
  import DocumentStore from 'orbit-db-docstore';
  import OrbitDBAddress from 'orbit-db/src/orbit-db-address';

  export type OrbitDBOptions = {
    directory?: string
    peerId?: string
    keystore?: Keystore
    cache?: any
    identity?: Identity
  }

  export type CreateStoreOptions = {
    accessController?: {
      write?: Array<string>
    }
    overwrite?: boolean
    replicate?: boolean
    meta?: { [key: string]: any }
  }

  export type OpenStoreOptions = {
    localOnly?: boolean
    create?: boolean
    type?: StoreType
    overwrite?: boolean
    replicate?: boolean
  }

  export type OverriddenOpenStoreOptions = {
    localOnly?: boolean
    overwrite?: boolean
    replicate?: boolean
  }

  export default class OrbitDB {

    static readonly databaseTypes: Array<StoreType>;
    readonly identity: Identity

    static createInstance(ipfs: IpfsApi, options?: OrbitDBOptions): Promise<OrbitDB>

    static isValidType(type: StoreType): boolean

    static addDatabaseType(type: StoreType, store: Store): void

    static getDatabaseTypes(type: StoreType, store: Store): Array<{ [key: string]: any }>

    static isValidAddress(address: any): boolean

    static parseAddress(address: any): OrbitDBAddress

    create<S extends Store>(name: string, type: StoreType, options?: CreateStoreOptions): Promise<S>

    disconnect(): Promise<void>

    stop(): Promise<void>

    determineAddress(name: string, type: StoreType, options?: CreateStoreOptions): Promise<OrbitDBAddress>

    open<S extends Store>(address: OrbitDBAddress | string, options?: OpenStoreOptions): Promise<S>

    keyvalue(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<KeyValueStore>

    kvstore(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<KeyValueStore>

    log<V>(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<EventStore<V>>

    eventlog<V>(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<EventStore<V>>

    feed<V>(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<FeedStore<V>>

    docs<V>(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<DocumentStore<V>>

    docstore<V>(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<DocumentStore<V>>

    counter(addressOrName: OrbitDBAddress | string, options?: OverriddenOpenStoreOptions): Promise<void>

  }
}

declare module 'orbit-db/src/orbit-db-address' {
  export default class OrbitDBAddress {
    readonly root: string;
    readonly path: string;

    constructor(root: string, path: string)

    static isValid(address: any): boolean

    static parse(address: any): OrbitDBAddress

    toString(): string
  }
}
