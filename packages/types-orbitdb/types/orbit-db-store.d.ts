declare module 'orbit-db-store' {
  import Identity from 'orbit-db-identity-provider/src/identity';
  import OrbitDBAddress from 'orbit-db/src/orbit-db-address';
  import {EventEmitter} from 'events';

  export type StoreType = 'counter' | 'eventlog' | 'feed' | 'docstore' | 'keyvalue' | string;

  export interface StoreOperation<V> {
    op: 'ADD' | 'DEL' | 'PUT' | 'COUNTER' | string,
    key: null | any,
    value: V
  }

  export type StoreEvent =
    'replicated'
    | 'replicate'
    | 'replicate.progress'
    | 'load'
    | 'load.progress'
    | 'ready'
    | 'write'
    | 'peer'
    | 'closed'
    | string;

  export interface StoreEventEmitter extends EventEmitter {
    addListener(event: StoreEvent | symbol, listener: (...args: any[]) => void): this;

    on(event: 'replicated', listener: (address: OrbitDBAddress) => void): this;

    once(event: 'replicated', listener: (address: OrbitDBAddress) => void): this;

    on(event: 'replicate', listener: (address: OrbitDBAddress) => void): this;

    once(event: 'replicate', listener: (address: OrbitDBAddress) => void): this;

    on(event: 'replicate.progress', listener: (address: OrbitDBAddress, hash: string, entry: any, progress: any, have: any) => void): this;

    once(event: 'replicate.progress', listener: (address: OrbitDBAddress, hash: string, entry: any, progress: any, have: any) => void): this;

    on(event: 'load', listener: (dbname: string) => void): this;

    once(event: 'load', listener: (dbname: string) => void): this;

    on(event: 'load.progress', listener: (address: OrbitDBAddress, hash: string, entry: any, progress: any, total: any) => void): this;

    once(event: 'load.progress', listener: (address: OrbitDBAddress, hash: string, entry: any, progress: any, total: any) => void): this;

    on(event: 'ready', listener: (dbname: string, heads: Array<any>) => void): this;

    once(event: 'ready', listener: (dbname: string, heads: Array<any>) => void): this;

    on(event: 'write', listener: (dbname: string, hash: string, entry: any) => void): this;

    once(event: 'write', listener: (dbname: string, hash: string, entry: any) => void): this;

    on(event: 'peer', listener: (peer: string) => void): this;

    once(event: 'peer', listener: (peer: string) => void): this;

    on(event: 'closed', listener: (dbname: string) => void): this;

    once(event: 'closed', listener: (dbname: string) => void): this;

    on(event: StoreEvent | symbol, listener: (...args: any[]) => void): this;

    once(event: StoreEvent | symbol, listener: (...args: any[]) => void): this;

    prependListener(event: StoreEvent | symbol, listener: (...args: any[]) => void): this;

    prependOnceListener(event: StoreEvent | symbol, listener: (...args: any[]) => void): this;

    removeListener(event: StoreEvent | symbol, listener: (...args: any[]) => void): this;

    off(event: StoreEvent | symbol, listener: (...args: any[]) => void): this;

    listeners(event: StoreEvent | symbol): Function[];

    rawListeners(event: StoreEvent | symbol): Function[];

    emit(event: StoreEvent | symbol, ...args: any[]): boolean;
  }

  export default class Store {

    readonly id: string;
    readonly dbname: string;
    readonly type: StoreType;
    readonly identity: Identity;
    readonly address: OrbitDBAddress;
    readonly events: StoreEventEmitter;

    close(): Promise<void>

    drop(): Promise<void>

    load(amount?: number, options?: { fetchEntryTimeout?: number }): Promise<void>

  }

}
