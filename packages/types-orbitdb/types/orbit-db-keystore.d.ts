declare module 'orbit-db-keystore' {
  import {Level} from 'level';
  import LRU from 'lru';

  export default class Keystore {

    _cache: LRU;
    _store: Level;

    constructor(store: string | any)

    static verify(signature: string, publicKey: string, data: any, v?: string): Promise<boolean>

    open(): Promise<void>

    close(): Promise<void>

    hasKey(id: string): Promise<boolean>

    createKey(id: string): Promise<any>

    getKey(id: string): Promise<any>

    sign(key: string, data: any): Promise<string>

    getPublic(key: string, data: any): Promise<string>
  }
}
