declare module 'orbit-db-eventstore' {
  import Store, {StoreOperation} from 'orbit-db-store';
  import {IdentityAsJson} from 'orbit-db-identity-provider/src/identity';

  export type IteratorOptions = {
    gt?: string
    gte?: string
    lt?: string
    lte?: string
    limit?: number
    reverse?: boolean
  }

  export interface EventStoreEntry<V> {
    hash: string
    id: string
    payload: StoreOperation<V>
    next: Array<any>
    v: number
    key: string
    identity: IdentityAsJson
    sig: string
  }

  export default class EventStore<V> extends Store {

    add(event: any): Promise<string>

    get(hash: string): EventStoreEntry<V>

    iterator(options?: IteratorOptions): {
      collect(): Array<EventStoreEntry<V>>
    } & Iterator<V>

  }
}
