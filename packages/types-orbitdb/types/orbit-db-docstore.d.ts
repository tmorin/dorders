declare module 'orbit-db-docstore' {
  import Store from 'orbit-db-store';

  export default class DocumentStore<V> extends Store {

    put(doc: any): Promise<string>

    get(key: string, caseSensitive?: boolean): Array<V>

    query(mapper: (d: V) => boolean, options?: { fullOp?: boolean }): Array<V>

    del(key: string): Promise<string>

  }

}
