declare module 'orbit-db-kvstore' {
  import Store from 'orbit-db-store';

  export default class KeyValueStore extends Store {

    put(key: string, value: any): Promise<string>

    set(key: string, value: any): Promise<string>

    get<T>(key: string): T

    del(key: string): Promise<string>

  }
}
