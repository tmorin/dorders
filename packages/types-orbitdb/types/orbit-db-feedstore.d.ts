declare module 'orbit-db-feedstore' {
  import EventStore from 'orbit-db-eventstore';

  export default class FeedStore<V> extends EventStore<V> {

    remove(hash: string): Promise<string>

    del(hash: string): Promise<string>

  }

}
