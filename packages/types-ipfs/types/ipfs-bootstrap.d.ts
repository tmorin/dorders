// https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/BOOTSTRAP.md
declare module 'ipfs' {
  import {MultiAddr} from 'multiaddr';

  export type BootstrapDefaultReturn = { Peers: Array<string> };

  export interface BootstrapApi {
    add(addr: MultiAddr | string, options?: Partial<DefaultOptions>): Promise<BootstrapDefaultReturn>

    reset(options?: Partial<DefaultOptions>): Promise<BootstrapDefaultReturn>

    list(options?: Partial<DefaultOptions>): Promise<BootstrapDefaultReturn>

    rm(addr: MultiAddr | string, options?: Partial<DefaultOptions>): Promise<BootstrapDefaultReturn>

    clear(options?: Partial<DefaultOptions>): Promise<BootstrapDefaultReturn>
  }

  export interface IpfsApi {
    readonly bootstrap: BootstrapApi
  }

}
