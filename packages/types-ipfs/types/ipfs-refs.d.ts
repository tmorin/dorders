//https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/REFS.md
declare module 'ipfs' {
  import CID from 'cids';

  export interface RefsOptions extends DefaultOptions {
    recursive: true
    unique: true
    format: string
    edges: true
    maxDepth: number
  }

  export type Ref = {
    ref: string
    err: Error | null
  }

  export interface RefsApi {
    refs(ipfsPath: CID | string, options?: Partial<RefsOptions>): AsyncIterable<Ref>

    local(options?: Partial<DefaultOptions>): AsyncIterable<Ref>
  }

  export interface IpfsApi {
    readonly refs: RefsApi
  }
}
