// https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/SWARM.md
declare module 'ipfs' {
  import {MultiAddr} from 'multiaddr';

  export interface SwarmPeersOptions extends DefaultOptions {
    direction: boolean
    streams: boolean
    verbose: boolean
    latency: boolean
  }

  export type SwarmAddrsItem = {
    id: string
    addrs: Array<MultiAddr>
  }

  export type SwarmPeersItem = {
    addr: MultiAddr
    peer: string
    // addrs: any
    // peer: string
    // latency: string
    // muxer: string
    // streams: Array<string>
    // direction: number
    // error: Error
    // rawPeerInfo: any
  }

  export interface SwarmApi {
    addrs(options?: Partial<DefaultOptions>): Promise<Array<SwarmAddrsItem>>

    connect(addr: MultiAddr | string, options?: Partial<DefaultOptions>): Promise<void>

    disconnect(addr: MultiAddr | string, options?: Partial<DefaultOptions>): Promise<void>

    localAddrs(options?: Partial<DefaultOptions>): Promise<Array<MultiAddr>>

    peers(options?: Partial<SwarmPeersOptions>): Promise<Array<SwarmPeersItem>>
  }

  export interface IpfsApi {
    readonly swarm: SwarmApi
  }

}
