// https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/MISCELLANEOUS.md
declare module 'ipfs' {
  import PeerId from 'peer-id';
  import CID from 'cids';
  import {MultiAddr} from 'multiaddr';

  export interface MiscDnsOptions extends DefaultOptions {
    recursive: true
  }

  export interface MiscPingOptions extends DefaultOptions {
    count: number
  }

  export type MiscPingResult = {
    success: boolean
    time: number
    text: string
  }

  export interface MiscResolveOptions extends DefaultOptions {
    recursive: boolean
    cidBase: string
  }

  export interface IpfsApi {

    id(options?: Partial<DefaultOptions>): Promise<{
      id: string
      publicKey: string
      addresses: Array<MultiAddr>
      agentVersion: string
      protocolVersion: string
    }>

    version(options?: Partial<DefaultOptions>): Promise<{
      version: string
      repo: number
      commit: string
    }>

    dns(domain: string, options?: Partial<MiscDnsOptions>): Promise<string>

    stop(options?: DefaultOptions): Promise<void>

    ping(peerId: PeerId | CID | string, options?: Partial<MiscPingOptions>): Promise<MiscPingResult>

    resolve(name: string, options?: Partial<MiscResolveOptions>): Promise<string>

    isOnline(): boolean
  }

}
