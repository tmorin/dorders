declare module 'multiaddr' {

  export type MultiaddrOptions = {
    family: 'ip4' | 'ipv6'
    host: string
    transport: string
    port: number
  }

  export type MultiaddrProtocol = {
    code: number
    size: number
    name: string
  }

  export type MultiaddrNodeAddress = {
    family: string
    address: string
    port: number
  }

  export class MultiAddr {
    static isName(addr: MultiAddr): boolean

    static resolve(addr: MultiAddr): Array<MultiAddr>

    static fromNodeAddress(addr: MultiaddrNodeAddress, transport: string): MultiAddr

    toOptions(): MultiaddrOptions

    toJSON(): string

    toString(): string

    inspect(): string

    protos(): Array<MultiaddrProtocol>

    protoCodes(): Array<number>

    protoNames(): Array<string>

    tuples(): Array<[number, Buffer]>

    stringTuples(): Array<[number, string | number]>

    encapsulate(addr: MultiAddr): MultiAddr

    decapsulate(addr: MultiAddr): MultiAddr

    decapsulateCode(code: number): MultiAddr

    getPeerId(): string | null

    getPath(): string | null

    equals(addr: MultiAddr): boolean

    nodeAddress(): MultiaddrNodeAddress

    isThinWaistAddress(addr: MultiAddr): boolean
  }

  export default function (value: string | Buffer | MultiAddr): MultiAddr

}
