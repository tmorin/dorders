// https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/PUBSUB.md
declare module 'ipfs' {

  export interface PubSubMsg {
    from: string,
    seqno: Buffer,
    data: Buffer,
    topicIDs: Array<string>
  }

  export interface PubsubApi {
    subscribe(topic: string, handler: (msg: PubSubMsg) => void, options?: Partial<DefaultOptions>): Promise<void>

    unsubscribe(topic: string, handler?: (msg: PubSubMsg) => void, options?: Partial<DefaultOptions>): Promise<void>

    publish(topic: string, data: Buffer | string, options?: Partial<DefaultOptions>): Promise<void>

    ls(options?: Partial<DefaultOptions>): Promise<Array<string>>

    peers(topic: string, options?: Partial<DefaultOptions>): Promise<Array<string>>
  }

  export interface IpfsApi {
    readonly pubsub: PubsubApi
  }

}

