// https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/FILES.md
declare module 'ipfs' {
  import CID from 'cids';
  import TypedArray = NodeJS.TypedArray;

  export type FileContent = Bytes | Bloby | string | Iterable<number> | Iterable<Bytes> | AsyncIterable<Bytes>;
  export type UnixTime = Date | { secs: number, nsecs?: number } | number[];
  export type Bytes = Buffer | ArrayBuffer | TypedArray;
  export type Bloby = Blob | File;
  export type FileObject = Partial<{
    path: string
    content: FileContent
    mode: number | string
    mtime: UnixTime
  }>;
  export type IpfsPath = string | CID;

  export interface FilesObjectFile {
    path: string
    cid: CID
    mode: number
    mtime: UnixTime
    size: number
  }

  export interface FilesListItem {
    depth: number
    name: string
    path: string
    size: number
    cid: CID
    type: string
    mode: number
    mtime: UnixTime
  }

  export interface FilesAddOptions extends DefaultOptions {
    chunker: 'size-' | 'rabin' | 'rabin-' | string
    cidVersion: number
    enableShardingExperiment: boolean
    hash: string
    hashAlg: string
    onlyHash: boolean
    pin: boolean
    progress: boolean
    rawLeaves: boolean
    shardSplitThreshold: number
    trickle: boolean
    wrapWithDirectory: boolean
  }

  export interface FilesCatOptions extends DefaultOptions {
    offset: number
    length: number
  }

  export interface FilesChmodOptions extends DefaultOptions {
    recursive: boolean
    flush: boolean
    hashAlg: string
    cidVersion: number
  }

  export interface FilesCpOptions extends DefaultOptions {
    parents: boolean
    flush: boolean
    hashAlg: string
    cidVersion: number
  }

  export interface FilesMkdirOptions extends DefaultOptions {
    parents: boolean
    mode: number
    mtime: UnixTime
    flush: boolean
    hashAlg: string
    cidVersion: number
  }

  export interface FilesStatOptions extends DefaultOptions {
    hash: boolean
    size: boolean
    withLocal: boolean
  }

  export interface FilesStatResult {
    cid: CID
    size: number
    cumulativeSize: number
    type: string | 'directory' | 'file'
    blocks: number
    withLocality: boolean
    local: boolean
    sizeLocal: number
  }

  export interface FilesTouchOptions extends DefaultOptions {
    mtime: Date | UnixTime | number
    flush: boolean
    hashAlg: string
    cidVersion: number
  }

  export interface FilesRmOptions extends DefaultOptions {
    recursive: boolean
    flush: boolean
    hashAlg: string
    cidVersion: number
  }

  export interface FilesReadOptions extends DefaultOptions {
    offset: number
    length: number
  }

  export interface FilesWriteOptions extends DefaultOptions {
    offset: number
    length: number
    create: boolean
    parents: boolean
    truncate: boolean
    rawLeaves: boolean
    mode: number
    mtime: Date | UnixTime | number
    flush: boolean
    hashAlg: string
    cidVersion: number
  }

  export interface FilesMvOptions extends DefaultOptions {
    parents: boolean
    flush: boolean
    hashAlg: string
    cidVersion: number
  }

  export interface FilesApi {
    ls(path: string, options?: Partial<DefaultOptions>): AsyncIterable<FilesListItem>

    chmod(ipfsPath: IpfsPath | CID, mode: string | number, options?: Partial<FilesChmodOptions>): Promise<void>

    cp(from: IpfsPath | CID, to: IpfsPath, options?: Partial<FilesCpOptions>): Promise<void>

    cp(from1: IpfsPath | CID, from2: IpfsPath | CID, to: string, options?: Partial<FilesCpOptions>): Promise<void>

    cp(from1: IpfsPath | CID, from2: IpfsPath | CID, from3: IpfsPath | CID, to: string, options?: Partial<FilesCpOptions>): Promise<void>

    cp(from1: IpfsPath | CID, from2: IpfsPath | CID, from3: IpfsPath | CID, from4: IpfsPath | CID, to: string, options?: Partial<FilesCpOptions>): Promise<void>

    mkdir(path: string, options?: Partial<FilesMkdirOptions>): Promise<void>

    stat(path: string, options?: Partial<FilesStatOptions>): Promise<FilesStatResult>

    touch(path: string, options?: Partial<FilesTouchOptions>): Promise<void>

    rm(path: string, options?: Partial<FilesRmOptions>): Promise<void>

    rm(path1: string, path2: string, options?: Partial<FilesRmOptions>): Promise<void>

    rm(path1: string, path2: string, path3: string, options?: Partial<FilesRmOptions>): Promise<void>

    rm(path1: string, path2: string, path3: string, path4: string, options?: Partial<FilesRmOptions>): Promise<void>

    read(path: string | CID, options?: Partial<FilesReadOptions>): AsyncIterable<Buffer>

    write(path: string | CID, content: string, options?: Partial<FilesWriteOptions>): Promise<void>

    mv(from: IpfsPath | CID, to: IpfsPath, options?: Partial<FilesMvOptions>): Promise<void>

    mv(from1: IpfsPath | CID, from2: IpfsPath | CID, to: string, options?: Partial<FilesMvOptions>): Promise<void>

    mv(from1: IpfsPath | CID, from2: IpfsPath | CID, from3: IpfsPath | CID, to: string, options?: Partial<FilesMvOptions>): Promise<void>

    mv(from1: IpfsPath | CID, from2: IpfsPath | CID, from3: IpfsPath | CID, from4: IpfsPath | CID, to: string, options?: Partial<FilesMvOptions>): Promise<void>

    flush(path: string, options?: Partial<DefaultOptions>): Promise<void>

  }

  export interface IpfsApi {
    readonly files: FilesApi

    add(data: Bytes | Bloby | string | FileObject | Iterable<any> | AsyncIterable<any>, options?: Partial<FilesAddOptions>): AsyncIterable<FilesObjectFile>

    cat(ipfsPath: IpfsPath, options?: Partial<FilesCatOptions>): AsyncIterable<Buffer>

    get(ipfsPath: IpfsPath, options?: Partial<DefaultOptions>): AsyncIterable<FilesObjectFile>

    ls(path: string): AsyncIterable<FilesListItem>
  }

}



