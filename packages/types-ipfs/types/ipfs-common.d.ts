declare module 'ipfs' {

  export interface DefaultOptions {
    timeout?: number
    signal?: AbortSignal
  }

}
