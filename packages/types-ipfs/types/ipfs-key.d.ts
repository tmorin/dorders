// https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/KEY.md
declare module 'ipfs' {

  export interface KeyApiGenOptions extends DefaultOptions {
    type: string | 'rsa' | 'ed25519'
    size: number
  }

  export type KeyApiItem = {
    id: string,
    name: string
  }

  export type KeyApiRenamedItem = {
    id: string,
    was: string
    now: string
    overwrite: boolean
  }

  export interface KeyApi {
    gen(name: string, options?: Partial<KeyApiGenOptions>): Promise<Array<KeyApiItem>>

    list(options?: Partial<DefaultOptions>): Promise<Array<KeyApiItem>>

    rm(name: string, options?: Partial<DefaultOptions>): Promise<KeyApiItem>

    rename(oldName: string, newName: string, options?: Partial<DefaultOptions>): Promise<KeyApiRenamedItem>

    export(name: string, password: string, options?: Partial<DefaultOptions>): Promise<string>

    import(name: string, pem: string, password: string, options?: Partial<DefaultOptions>): Promise<KeyApiItem>
  }

  export interface IpfsApi {
    readonly key: KeyApi
  }

}
