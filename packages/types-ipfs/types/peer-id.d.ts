// https://github.com/libp2p/js-peer-id
declare module 'peer-id' {

  export default class PeerId {

    readonly id: Buffer;
    readonly privKey: any;
    readonly pubKey: any;

    constructor(id: Buffer, privKey?: any, pubKey?: any)

    static create(options?: { bits: number, keyType: string }): Promise<PeerId>

    static createFromHexString(str: string): PeerId

    static createFromBytes(buf: Buffer): PeerId

    static createFromB58String(str: string): PeerId

    static createFromPubKey(buf: Buffer): Promise<PeerId>

    static createFromPrivKey(buf: Buffer): Promise<PeerId>

    static createFromJSON(obj: { id: string, pubKey: string, privKey: string }): Promise<PeerId>

    static createFromProtobuf(buf: Buffer): PeerId

    isPeerId(id?: PeerId): boolean

    toHexString(): string

    toBytes(): Buffer

    toB58String(): string

    toJSON(): { id: string, pubKey: string, privKey: string }

    marshal(excludePrivateKey?: boolean): Buffer

    marshalPubKey(): any

    toPrint(): string

    isEqual(id?: PeerId): boolean
  }
}
