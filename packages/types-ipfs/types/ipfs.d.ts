// https://github.com/ipfs/js-ipfs/blob/master/packages/ipfs/docs/MODULE.md
// https://github.com/ipfs/js-ipfs/blob/master/docs/CONFIG.md
declare module 'ipfs' {

  export type ConfigProfile =
    'server'
    | 'local-discovery'
    | 'test'
    | 'default-networking'
    | 'lowpower'
    | 'default-power';

  export interface IpfsOptions {
    repo?: string
    repoAutoMigrate?: boolean
    init?: boolean | {
      emptyRepo?: boolean
      bits?: number
      privateKey?: string
      pass?: string
      profiles?: Array<ConfigProfile>
      allowNew?: boolean
    }
    start?: boolean
    pass?: string
    silent?: boolean
    relay?: {
      enabled?: boolean
      hop?: {
        enabled?: boolean
        active?: boolean
      }
    }
    offline?: boolean,
    preload?: {
      enabled?: boolean,
      addresses?: Array<string>
    }
    EXPERIMENTAL?: {
      ipnsPubsub?: boolean
      sharding?: boolean
      [key: string]: any
    }
    config?: {
      Addresses?: {
        API?: string
        Delegates?: Array<string>
        Gateway?: string
        Swarm?: Array<string>
      }
      Bootstrap?: Array<string>
      Datastore?: {
        Spec?: any
      }
      Discovery?: {
        MDNS?: {
          Enabled?: boolean
          Interval?: number
        },
        webRTCStar?: {
          Enabled?: boolean
        }
      }
      Identity?: {
        PeerID?: any
        PrivKey?: any
      }
      Keychain?: any
      Pubsub?: {
        Router?: 'gossipsub' | 'floodsub' | string
        Enabled?: boolean
      }
      Swarm?: {
        ConnMgr?: {
          LowWater?: number
          HighWater?: number
        }
      }
    }
    ipld?: any
    libp2p?: any
  }

  export function create(options?: IpfsOptions): Promise<IpfsApi>
}
