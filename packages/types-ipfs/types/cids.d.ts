declare module 'cids' {
  export type CIDVersion = 0 | 1
  export default class CID {
    readonly version: CIDVersion;
    readonly codec: string;
    readonly multihash: Buffer;
    readonly multibaseName: string;
    readonly buffer: Buffer;
    readonly prefix: string;

    constructor(baseEncodedString: string | Buffer)

    constructor(version: CIDVersion, codec: string, multihash: Buffer, multibaseName: string)

    static isCID(cid: any): boolean

    static validateCID(cid: CID): boolean

    toV0(): CID

    toV1(): CID

    toBaseEncodedString(base: string): string

    toString(): string

    equals(cid: CID): boolean

  }
}
