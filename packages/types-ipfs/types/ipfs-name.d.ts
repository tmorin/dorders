//https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/REFS.md
declare module 'ipfs' {
  import CID from 'cids';

  export interface NamePublishOptions extends DefaultOptions {
    resolve: true
    lifetime: string
    ttl: string
    key: string
    allowOffline: boolean
  }

  export interface NameResolveOptions extends DefaultOptions {
    recursive: true
    nocache: boolean
  }

  export type NamePublishResult = {
    name: string
    value: string
  }

  export interface NameApi {
    readonly pubsub: {
      cancel(name: string, options?: Partial<DefaultOptions>): Promise<{ canceled: boolean }>

      state(options?: Partial<DefaultOptions>): Promise<{ enabled: boolean }>

      subs(options?: Partial<DefaultOptions>): Promise<Array<string>>
    }

    publish(value: CID | string, options?: Partial<NamePublishOptions>): Promise<NamePublishResult>

    resolve(value: string, options?: Partial<NameResolveOptions>): AsyncIterable<string>

  }

  export interface IpfsApi {
    readonly name: NameApi
  }
}
