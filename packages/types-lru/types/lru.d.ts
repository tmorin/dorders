declare module 'lru' {

  export default class LRU {

    readonly length: number;
    readonly keys: Array<string>;

    constructor(length: number, options?: {
      max?: number
      maxAge?: number
    })

    set<T>(key: string, value: T): T

    get<T>(key: string): T

    peek<T>(key: string): T | undefined

    remove<T>(key: string): T | undefined

    clear()

    on(event: 'evict', callback: (item: { key: string, value: any }) => void)

  }

}
