import {Event} from '@dorders/framework';
import {LocalMessageBus} from './LocalMessageBus';
import {LogpleaseLoggerFactory} from '@dorders/infra-logger-logplease';

jest.setTimeout(15000);

class EventA extends Event {
  constructor() {
    super(undefined, EventA.name);
  }
}

class EventB extends Event {
  constructor() {
    super(undefined, EventB.name);
  }
}

describe('LocalMessageBus/event', function () {

  it('should react on events', async function () {
    const bus = new LocalMessageBus(new LogpleaseLoggerFactory());
    const waitForTwoEventA = new Promise(resolve => {
      let cnt = 0;
      bus.on('EventA', () => {
        cnt += 1;
        if (cnt === 2) {
          resolve();
        }
      })
    });
    await bus.publish(new EventA());
    await bus.publish(new EventB());
    await bus.publish(new EventA());
    await waitForTwoEventA;
  });

  it('should react once on event', async function () {
    const bus = new LocalMessageBus(new LogpleaseLoggerFactory());
    let cnt = 0;
    bus.once('EventA', () => cnt += 1);
    const waitForEventB = new Promise(resolve => bus.once('EventB', resolve));
    await bus.publish(new EventA());
    await bus.publish(new EventA());
    await bus.publish(new EventB());
    await waitForEventB;
    expect(cnt).toEqual(1);
  });

});
