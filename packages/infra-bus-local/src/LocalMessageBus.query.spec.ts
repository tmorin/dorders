import {Query, QueryHandler, Result} from '@dorders/framework';
import {LocalMessageBus} from './LocalMessageBus';
import {LogpleaseLoggerFactory} from '@dorders/infra-logger-logplease';

jest.setTimeout(15000);

class QueryA extends Query {
  constructor() {
    super(undefined, 'QueryA');
  }
}

class ResultA extends Result {
  constructor() {
    super(undefined, 'ResultA');
  }
}

class QueryAHandler extends QueryHandler<QueryA, ResultA> {
  async handle(query: QueryA): Promise<ResultA> {
    return new ResultA();
  }
}

describe('LocalMessageBus/query', function () {

  it('should register and call query', async function () {
    const bus = new LocalMessageBus(new LogpleaseLoggerFactory());
    bus.registerQueryHandler(QueryA.name, new QueryAHandler());
    const resultA = await bus.call(new QueryA());
    expect(resultA.name).toEqual('ResultA');
  });

});
