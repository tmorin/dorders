import {Command, CommandHandler, Event} from '@dorders/framework';
import {LocalMessageBus} from './LocalMessageBus';
import {LogpleaseLoggerFactory} from '@dorders/infra-logger-logplease';

jest.setTimeout(15000);

class CommandA extends Command {
  constructor() {
    super(undefined, 'CommandA');
  }
}

class EventA extends Event {
  constructor() {
    super(undefined, 'EventA');
  }
}

class CommandAHandler extends CommandHandler<CommandA, EventA> {
  async handle(command: Command): Promise<Array<Event>> {
    return [new EventA()];
  }
}

describe('LocalMessageBus/command', function () {

  it('should register and execute command', async function () {
    const bus = new LocalMessageBus(new LogpleaseLoggerFactory());
    bus.registerCommandHandler(CommandA.name, new CommandAHandler());
    const waitForEventA = new Promise(resolve => bus.once('EventA', resolve));
    const [eventA] = await bus.execute(new CommandA());
    expect(eventA.name).toEqual('EventA');
    const eventABis = await waitForEventA;
    expect(eventABis).toEqual(eventA);
  });

});
